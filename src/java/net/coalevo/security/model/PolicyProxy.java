/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import net.coalevo.foundation.model.Action;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * This class implements a proxy for a {@link Policy}.
 * <p/>
 * The idea of this proxy is to handle the policy, taking
 * into account the possibility of a disappearing and reappearing {@link PolicyService}.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PolicyProxy
    implements Policy, ServiceAgentProxyListener {

  private Logger m_Log;
  private Marker m_LogMarker;

  private BundleContext m_BundleContext;
  private String m_Identifier;
  private String m_PolicyURL;

  // Services and proxies
  private ServiceAgentProxy m_ServiceAgent;
  private Policy m_Policy;
  private PolicyService m_PolicyService;
  private PolicyXMLService m_PolicyXMLService;

  // Wait latches
  private CountDownLatch m_PolicyLatch;
  private CountDownLatch m_PolicyServiceLatch;
  private CountDownLatch m_PolicyXMLServiceLatch;

  public PolicyProxy(String id, String purl, ServiceAgentProxy sa, Logger log) {
    m_Identifier = id;
    m_ServiceAgent = sa;
    m_Log = log;
    m_LogMarker = MarkerFactory.getMarker("PolicyProxy::" + id);
    m_PolicyURL = purl;
  }//constructor

  public PolicyService getPolicyService() {
    try {
      m_PolicyServiceLatch.await();
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_PolicyService;
  }//getPolicyService

  public PolicyXMLService getPolicyXMLService() {
    try {
      m_PolicyXMLServiceLatch.await();
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_PolicyXMLService;
  }//getPolicyXMLService

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public Set getActions() {
    try {
      m_PolicyLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "getActions()", e);
    }
    return m_Policy.getActions();
  }//getActions;

  public AuthorizationRule getRuleFor(Action a) {
    try {
      m_PolicyLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "getRuleFor()", e);
    }
    return m_Policy.getRuleFor(a);
  }//getRuleFor

  public boolean checkAuthorization(Authorizations auth, Action a)
      throws NoSuchActionException {
    try {
      m_PolicyLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "checkAuthorization()", e);
    }
    return m_Policy.checkAuthorization(auth, a);
  }//checkAuthorization

  public void ensureAuthorization(Authorizations auth, Action a)
      throws AuthorizationException, NoSuchActionException {
    try {
      m_PolicyLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker, "ensureAuthorization()", e);
    }
    m_Policy.ensureAuthorization(auth, a);
  }//ensureAuthorization

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    m_PolicyServiceLatch = new CountDownLatch(1);
    m_PolicyXMLServiceLatch = new CountDownLatch(1);
    m_PolicyLatch = new CountDownLatch(1);

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(objectclass=" + PolicyService.class.getName() + ")" +
            "   (objectclass=" + PolicyXMLService.class.getName() + ")" +
            ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    //Listen changes after first time
    m_ServiceAgent.getAuthenticPeer();
    m_ServiceAgent.addListener(this);
    return true;
  }//prepare

  public void deactivate() {
    //Services
    if (m_PolicyService != null && m_Policy != null) {
      try {
        m_PolicyService.releasePolicy(m_Policy);
      } catch (Exception ex) {
        m_Log.error(m_LogMarker, "deactivate()", ex);
      }
      m_PolicyService = null;
      m_Policy = null;
    }

    //latches
    m_PolicyServiceLatch = null;
    m_PolicyXMLServiceLatch = null;
    m_PolicyLatch = null;

    m_Identifier = null;
    m_PolicyURL = null;
    m_BundleContext = null;
  }//deactivate

  private void initPolicy() {
    Thread t = new Thread(new Runnable() {
      public void run() {
        try {

          if (getPolicyService().isPolicyAvailable(getIdentifier())) {
            m_Policy = getPolicyService().leasePolicy(m_ServiceAgent.getAuthenticPeer(), getIdentifier());
            m_PolicyLatch.countDown();
          } else {
            try {
              m_Log.debug(m_LogMarker, "initPolicy()::start::" + m_PolicyURL);
              //load from embedded file
              URL policy = m_BundleContext.getBundle().getEntry(m_PolicyURL);
              Policy p = getPolicyXMLService().fromXML(new InputStreamReader(policy.openStream()));
              //store it
              for (Iterator iterator = p.getActions().iterator(); iterator.hasNext();) {
                Action a = (Action) iterator.next();
                m_Policy = m_PolicyService.createPolicy(m_ServiceAgent.getAuthenticPeer(), getIdentifier());
                boolean b = m_PolicyService.putPolicyEntry(m_ServiceAgent.getAuthenticPeer(), m_Policy, a, p.getRuleFor(a));
                if (!b) {
                  m_Log.error(m_LogMarker, "action" + a.getIdentifier());
                }
              }
              m_PolicyLatch.countDown();              
              m_Log.debug(m_LogMarker, "initPolicy()::end::" + m_PolicyURL);
            } catch (Exception ex) {
              m_Log.error("activate()", ex);
            }
          }


        } catch (Exception ex) {
          m_Log.error("activate()", ex);
        }
      }//Runnable
    });//Thread
    t.setContextClassLoader(this.getClass().getClassLoader());
    t.start();
  }//initPolicy

  public void updatedServiceAgent() {
    initPolicy();
  }//updated

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof PolicyService) {
            m_PolicyService = (PolicyService) o;
            m_PolicyServiceLatch.countDown();
            initPolicy();
            m_Log.debug(m_LogMarker, "serviceChanged()::registered::PolicyService");
          } else if (o instanceof PolicyXMLService) {
            m_PolicyXMLService = (PolicyXMLService) o;
            m_PolicyXMLServiceLatch.countDown();
            m_Log.debug(m_LogMarker, "serviceChanged()::registered::PolicyXMLService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof PolicyService) {
            m_PolicyService = null;
            m_Policy = null;
            m_PolicyServiceLatch = new CountDownLatch(1);
            m_PolicyLatch = new CountDownLatch(1);
            m_Log.debug(m_LogMarker, "serviceChanged()::unregistered::PolicyService");
          } else if (o instanceof PolicyXMLService) {
            m_PolicyXMLService = null;
            m_PolicyXMLServiceLatch = new CountDownLatch(1);
            m_Log.debug(m_LogMarker, "serviceChanged()::unregistered::PolicyXMLService");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class PolicyProxy
