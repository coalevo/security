/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

/**
 * Defines an <tt>AuthorizationRule</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface AuthorizationRule {

  /**
   * Checks if the given {@link Authorizations} are sufficient
   * by evaluation of this <tt>AuthorizationRule</tt>.
   *
   * @param a an {@link Authorizations} set.
   * @return true if authorized, false otherwise.
   */
  public boolean isAuthorized(Authorizations a);

  /**
   * Returns the SARL statement that represents this
   * <tt>AuthorizationRule</tt>.
   */
  public String toSARL();

}//interface AuthorizationRule
