/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import java.util.Set;

/**
 * Defines a container for <tt>Authorizations</tt>.
 * <p/>
 * The two types of authorizations defined in the
 * Coalevo platform are:
 * <ol>
 * <li>Permissions (instance of {@link Permission})</li>
 * <li>Roles (instances of {@link Role}</li>
 * </ol>
 * Permissions are actually associated with roles,
 * and resolved into a set according to a simple alogrithm.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Authorizations {

  /**
   * Checks if this <tt>Agent</tt> has
   * a <em>permission</em> specified by the given name.
   *
   * @param id a <tt>String</tt> that identifies a <em>permission</em>.
   * @return true if allowed and not denied, false otherwise.
   */
  public boolean hasPermission(String id);

  /**
   * Checks if this <tt>Agent</tt> has a <em>role</em>
   * specified by the given identifier.
   *
   * @param id a <tt>String</tt> identifier for a {@link Role} instance.
   * @return true if this <tt>Authorizations</tt> instance contains
   *         the given role.
   */
  public boolean hasRole(String id);

  /**
   * Returns an unmodifiable set of the names of all roles
   * of the corresponding agent.
   *
   * @return an unmodifiable <tt>Set</tt> of <tt>String</tt> elements.
   */
  public Set<String> getRolenames();

}//interface Authorizations
