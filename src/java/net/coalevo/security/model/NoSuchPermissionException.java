/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import java.util.NoSuchElementException;

/**
 * Signals that an identifier has been specified that does
 * not map to any known {@link Permission}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NoSuchPermissionException
    extends NoSuchElementException {

  /**
   * Constructs a new instance with the message set to
   * the given unknown permission identifier.
   *
   * @param pid an identifier that does not map to any
   *            known {@link Permission}.
   */
  public NoSuchPermissionException(String pid) {
    super(pid);
  }//constructor

}//class NoSuchPermissionException
