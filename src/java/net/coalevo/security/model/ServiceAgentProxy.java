/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.model.Service;
import net.coalevo.security.service.SecurityService;
import org.osgi.framework.*;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.Set;
import java.util.HashSet;

/**
 * This class implements a proxy for an authenticated {@link ServiceAgent}.
 * <p/>
 * The idea of this proxy is to handle service authentication, taking
 * into account the possibility of a disappearing and reappearing {@link SecurityService}.
 * 
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ServiceAgentProxy implements ServiceAgent {

  private Service m_Service;
  private ServiceAgent m_ServiceAgent;
  private SecurityService m_SecurityService;
  private CountDownLatch m_AvailLatch;
  private BundleContext m_BundleContext;
  private Logger m_Log;
  private Marker m_LogMarker;
  private Set<ServiceAgentProxyListener> m_Listeners;

  public ServiceAgentProxy(Service s, Logger log) {
    m_Log = log;
    m_LogMarker = MarkerFactory.getMarker("ServiceAgentProxy::" + s.getIdentifier());
    m_Service = s;
    m_Listeners = new HashSet<ServiceAgentProxyListener>();
  }//ServiceAgentProxy

  public String getIdentifier() {
    return getAuthenticPeer().getIdentifier();
  }//getIdentifier

  public AgentIdentifier getAgentIdentifier() {
    return getAuthenticPeer().getAgentIdentifier();
  }//getAgentIdentifier

  public SecurityService getSecurityService() {
     try {
      m_AvailLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker,"getAuthenticPeer()", e);
    }
    return m_SecurityService;
  }//getSecurityService

  public ServiceAgent getAuthenticPeer() {
    try {
      m_AvailLatch.await();
    } catch (InterruptedException e) {
      m_Log.error(m_LogMarker,"getAuthenticPeer()", e);
    }
    return m_ServiceAgent;
  }//getAuthenticPeer

  public void addListener(ServiceAgentProxyListener l) {
    synchronized (m_Listeners) {
      m_Listeners.add(l);
    }
  }//addListener

  public void removeListener(ServiceAgentProxyListener l) {
    synchronized(m_Listeners) {
      m_Listeners.remove(l);
    }
  }//removeListener

  private void notifyListeners() {
    synchronized (m_Listeners) {
      for(ServiceAgentProxyListener l:m_Listeners) {
        l.updatedServiceAgent();
      }
    }
  }//notifyListeners

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    m_AvailLatch = new CountDownLatch(1);
    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(objectclass=" + SecurityService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    if (m_SecurityService != null && m_ServiceAgent != null) {
      try {
        m_SecurityService.invalidateAuthentication(m_ServiceAgent);
      } catch (Exception ex) {
        m_Log.error(m_LogMarker, "deactivate()", ex);
      }
      m_SecurityService = null;
      m_ServiceAgent = null;
    }
    m_Listeners.clear();
    m_BundleContext = null;
  }//deactivate

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = (SecurityService) o;
            //authenticate service agent
            try {
              m_ServiceAgent = m_SecurityService.authenticate(m_Service);
            } catch (AuthenticationException ex) {
              m_Log.error(m_LogMarker, "serviceChanged()",ex);
              m_SecurityService = null;
              m_BundleContext.ungetService(sr);
              return;
            }
            m_Log.debug(m_LogMarker, "serviceChanged()::registered::SecurityService,ServiceAgent");
            m_AvailLatch.countDown();
            notifyListeners();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = null;
            m_ServiceAgent = null;
            m_AvailLatch = new CountDownLatch(1);
            m_Log.debug(m_LogMarker, "serviceChanged()::unregistered::SecurityService,ServiceAgent");
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

}//class ServiceAgentProxy
