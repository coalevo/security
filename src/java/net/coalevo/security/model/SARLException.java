/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

/**
 * Signals that a given SARL statement could not be
 * converted into an {@link AuthorizationRule} instace due
 * to a parser error.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SARLException
    extends Exception {

  /**
   * Constructs a new <tt>SARLException</tt> with the <tt>Throwable</tt>
   * that caused this <tt>SARLException</tt>.
   *
   * @param cause the root cause <tt>Throwable</tt>.
   */
  public SARLException(Throwable cause) {
    super(cause);
  }//constructor

}//class SARLException
