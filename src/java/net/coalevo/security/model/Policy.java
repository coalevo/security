/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.Agent;
import net.coalevo.security.service.PolicyService;

import java.util.Set;

/**
 * Defines the interface for a <tt>Policy</tt>.
 * <p/>
 * Policies are used for authorization purposes when
 * actions of a service it is associated with are invoked.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Policy {

  /**
   * Returns the {@link PolicyService} this <tt>Policy</tt>
   * originated from.
   *
   * @return a {@link PolicyService} instance.
   */
  public PolicyService getPolicyService();

  /**
   * Returns the identifier of this <tt>Policy</tt>.
   *
   * @return the identifier as <tt>String</tt>.
   */
  public String getIdentifier();

  /**
   * Returns a <tt>Set</tt> of {@link Action} instances that map
   * to {@link AuthorizationRule} instances.
   *
   * @return an unmodifiable <tt>Set</tt> of {@link Action} instances.
   */
  public Set getActions();

  /**
   * Returns the {@link AuthorizationRule} for the given {@link Action}.
   *
   * @param a an {@link Action} instance obtained through {@link #getActions()}.
   * @return the {@link AuthorizationRule} associated with the given {@link Action}.
   */
  public AuthorizationRule getRuleFor(Action a);

  /**
   * Checks if the given {@link Agent} is authorized
   * to use an given service <tt>Action</tt>.
   *
   * @param auth an {@link Authorizations} set.
   * @param a    an {@link Action} instance.
   * @return true if authorized, false otherwise.
   * @throws NoSuchActionException if the given {@link Action}
   *                               is unknown to this <tt>Policy</tt>.
   */
  public boolean checkAuthorization(Authorizations auth, Action a)
      throws NoSuchActionException;

  /**
   * Ensures that the given {@link Agent} is authorized
   * to use an given service <tt>Action</tt>.
   *
   * @param auth an {@link Authorizations} set.
   * @param a    an {@link Action} instance.
   * @throws AuthorizationException if the calling {@link Agent}
   *                                is not authorized.
   * @throws NoSuchActionException  if the given {@link Action}
   *                                is unknown to this <tt>Policy</tt>.
   */
  public void ensureAuthorization(Authorizations auth, Action a)
      throws AuthorizationException, NoSuchActionException;


}//interface PolicyService
