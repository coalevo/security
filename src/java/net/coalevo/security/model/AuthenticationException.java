/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

/**
 * Signals that an authentication process failed to
 * authenticate the corresponding {@link net.coalevo.foundation.model.Agent}, or
 * that a non-authenticated {@link net.coalevo.foundation.model.Agent} instance
 * is used for an action that requires authorization.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see net.coalevo.security.service.SecurityService#authenticate(net.coalevo.foundation.model.AgentIdentifier,String)
 * @see net.coalevo.security.service.SecurityService#authenticate(net.coalevo.foundation.model.Service)
 * @see net.coalevo.security.service.SecurityService#getAuthorizations(net.coalevo.foundation.model.Agent)
 */
public class AuthenticationException
    extends SecurityException {

  public AuthenticationException() {
    super();
  }

  public AuthenticationException(String msg) {
    super(msg);
  }//AuthenticationException

}//class AuthenticationException
