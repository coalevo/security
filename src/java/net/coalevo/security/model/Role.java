/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import net.coalevo.foundation.model.Identifiable;

/**
 * Defines the interface for a <tt>Role</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Role extends Identifiable {

  /**
   * Returns the identifier of this <tt>Role</tt>.
   *
   * @return the identifier as <tt>String</tt>.
   * @see #toString()
   */
  public String getIdentifier();

  /**
   * Tests if this <tt>Role</tt> is equal to the
   * given one, based on its identifier.
   *
   * @param o a <tt>RoleImpl</tt> instance.
   * @return true if equal, false otherwise.
   */
  public boolean equals(Object o);

  /**
   * Returns the identifier of this <tt>Role</tt>.
   *
   * @return an identifier as <tt>String</tt>.
   * @see #getIdentifier()
   */
  public String toString();

}//interface Role
