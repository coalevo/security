/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.model;

import java.util.NoSuchElementException;

/**
 * Signals that an identifier has been specified that does
 * not map to any known {@link Policy} or a {@link Policy}
 * instance has been provided that does not belong to
 * the used {@link net.coalevo.security.service.PolicyService}
 * instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NoSuchPolicyException extends NoSuchElementException {

  /**
   * Constructs a new instance with the message set
   * to the unknown policy identifier.
   *
   * @param p a {@link Policy} instance.
   */
  public NoSuchPolicyException(Policy p) {
    super(p.getIdentifier());
  }//constructor

  /**
   * Constructs a new instance with the message set
   * to the unknown policy identifier.
   *
   * @param id a policy identifier as <tt>String</tt>.
   */
  public NoSuchPolicyException(String id) {
    super(id);
  }//constructor

}//class NoSuchPolicyException
