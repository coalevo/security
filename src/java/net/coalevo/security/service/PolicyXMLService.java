/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.service;

import net.coalevo.foundation.model.Service;
import net.coalevo.security.model.Policy;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Instances of this service provide
 * mapping between XML and {@link Policy} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PolicyXMLService
    extends Service {

  /**
   * Serializes the given {@link Policy} instance
   * as XML to the given writer.
   *
   * @param w a <tt>Writer</tt>.
   * @param p a {@link Policy} instance.
   * @throws IOException if the I/O fails.
   */
  public void toXML(Writer w, Policy p)
      throws IOException;

  /**
   * Convenience method that will return the serialized
   * {@link Policy} instance as XML in a <tt>String</tt>.
   *
   * @param p a {@link Policy} instance.
   * @return a String containing the policy as XML.
   * @see #toXML(Writer,Policy)
   * @throws IOException if the I/O fails.
   */
  public String toXML(Policy p) throws IOException;


  /**
   * Reads an XML serialized {@link Policy} instance from a
   * given <tt>Reader</tt>.
   *
   * @param r a <tt>Reader</tt>.
   * @return the unserialized {@link Policy} instance.
   * @throws IOException if the I/O fails.
   */
  public Policy fromXML(Reader r) throws IOException;

  /**
   * Convenience method that reads an XML serialized {@link Policy}
   * instance from a given <tt>String</tt>.
   *
   * @param str a <tt>String</tt> containing the XML.
   * @return the unserialized {@link Policy} instance.
   * @throws IOException if the I/O fails.
   */
  public Policy fromXML(String str) throws IOException;

}//interface PolicyXMLService