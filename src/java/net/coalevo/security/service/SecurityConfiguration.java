/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.service;

import net.coalevo.security.model.Deny;
import net.coalevo.security.model.Permission;
import net.coalevo.security.model.Permit;
import net.coalevo.security.model.Role;

/**
 * Defines a tagging interface for the security bundle configuration.
 * <p/>
 * The interface full classified name will be the PID for the configuration
 * dictionary that corresponds to various parts of the bundle.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface SecurityConfiguration {

  /**
   * Defines the key for the configuration attribute that stores
   * the data source of this bundle.
   */
  public static final String CONFIG_DATASOURCE_KEY = "datasource";

  /**
   * Defines the key for the configuration attribute that stores the
   * name of the <tt>root</tt> user (the first admin to setup the system).
   */
  public static final String CONFIG_ROOT_KEY = "root";

  /**
   * Defines the key for the configuration attribute that stores the
   * name of the base admin <tt>role</tt>.
   */
  public static final String CONFIG_ADMINROLE_KEY = "role.admin";

  /**
   * Defines an emergency default for the default admin role name.
   * Note that this default should usually be stored in the MetaType
   * information for this bundle.
   */
  public static final String CONFIG_ADMINROLE_DEFAULT = "Admin";

  /**
   * Defines the key for the configuration attribute that stores the
   * name of the base user <tt>role</tt>.
   */
  public static final String CONFIG_USERROLE_KEY = "role.user";

  /**
   * Defines an emergency default for the default user role name.
   * Note that this default should usually be stored in the MetaType
   * information for this bundle.
   */
  public static final String CONFIG_USERROLE_DEFAULT = "User";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the {@link Role} instance LRU cache.
   */
  public static final String CONFIG_ROLECACHE_KEY = "cache.roles";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the {@link Permission} instance LRU cache.
   * Permission instances are used for administration purposes, and
   * their meaning is the neutral existence of a specific permission
   * that might be permitted or denied through permits or denies.
   */
  public static final String CONFIG_PERMISSIONCACHE_KEY = "cache.permissions";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the {@link Permit} instance LRU cache.
   */
  public static final String CONFIG_PERMITCACHE_KEY = "cache.permits";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the {@link Deny} instance LRU cache.
   */
  public static final String CONFIG_DENYCACHE_KEY = "cache.denies";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the LRU cache for resolved role permissions (i.e. role -> permissions).
   */
  public static final String CONFIG_ROLEPERMISSIONCACHE_KEY = "cache.rolepermissions";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the LRU cache for {@link net.coalevo.foundation.model.UserAgent} instances.
   */
  public static final String CONFIG_USERAGENTCACHE_KEY = "cache.useragents";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the LRU cache for {@link net.coalevo.foundation.model.ServiceAgent} instances.
   */
  public static final String CONFIG_SERVICEAGENTCACHE_KEY = "cache.serviceagents";

  /**
   * Defines the key for the configuration attribute that stores
   * the size of the LRU cache for authentication information (i.e. credentials
   * cached in memory after successful authentications).
   */
  public static final String CONFIG_AUTHENTICATIONCACHE_KEY = "cache.authentications";

  /**
   * Defines the key for the configuration attribute that stores
   * the number of connections in the database connection pool.
   */
  public static final String CONFIG_CONNECTIONPOOLSIZE_KEY = "connections.poolsize";

}//interface SecurityConfiguration
