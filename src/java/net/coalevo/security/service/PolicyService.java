/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.service;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.Service;
import net.coalevo.security.model.*;

import java.util.Set;

/**
 * This interface defines a service for managing
 * policies.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PolicyService
    extends Service {

  /**
   * Returns the base {@link AuthorizationRule} for
   * the system.
   *
   * @return an {@link AuthorizationRule} instance.
   */
  public AuthorizationRule getBaseAuthorizationRule();

  /**
   * Return an {@link AuthorizationRule} parsed from the given
   * SARL statement.
   *
   * @param stmt a SARL rule statement.
   * @return an {@link AuthorizationRule} instance
   * @throws SARLException if a parser error occurs.
   */
  public AuthorizationRule createAuthorizationRule(String stmt)
      throws SARLException;

  /**
   * Lists all available policies.
   *
   * @return an unmodifiable {@link Set} holding
   *         {@link Policy} identifiers.
   */
  public Set<String> listAvailablePolicies();

  /**
   * Tests if a {@link Policy} with the given
   * identifier is available.
   *
   * @param identifier the identifier as <tt>String</tt>.
   * @return true if available, false otherwise.
   */
  public boolean isPolicyAvailable(String identifier);

  /**
   * Returns the leased {@link Policy} instance associated with the
   * given identifier.
   *
   * @param caller     the calling {@link Agent}.
   * @param identifier the identifier as <tt>String</tt>.
   * @return the {@link Policy} instance associated with the given identifier or
   *         null if the identifier is not associated with any policy.
   * @throws SecurityException if the calling {@link Agent}
   *                           is not authorized or authentic.
   * @throws NoSuchPolicyException if the given identifier has no corresponding policy.
   */
  public Policy leasePolicy(Agent caller, String identifier)
      throws SecurityException, NoSuchPolicyException;

  /**
   * Releases the given {@link Policy} instance.
   *
   * @param p a previously leased {@link Policy} instance.
   */
  public void releasePolicy(Policy p);

  /**
   * Create a {@link Policy} instance.
   * <p/>
   * This is a factory for new  {@link Policy} unassociated
   * instances.
   *
   * @param caller     the calling {@link Agent}.
   * @param identifier the identifier as <tt>String</tt>.
   * @return the {@link Policy} instance associated with the given identifier.
   * @throws SecurityException if the calling {@link Agent}
   *                           is not authorized or authentic.
   */
  public Policy createPolicy(Agent caller, String identifier)
      throws SecurityException;

  /**
   * Deletes an existing {@link Policy} instance, if it
   * is not referenced currently.
   *
   * @param caller     caller the calling {@link Agent}.
   * @param identifier the identifier as <tt>String</tt>.
   * @return true if deleted, false otherwise.
   * @throws SecurityException if the calling {@link Agent}
   *                           is not authorized or authentic.
   * @throws NoSuchPolicyException if the given identifier has no corresponding policy.
   */
  public boolean deletePolicy(Agent caller, String identifier)
      throws SecurityException, NoSuchPolicyException;


  /**
   * Adds or updates a policy entry associated with a specific
   * {@link Action} instance.
   * <p/>
   * The calling {@link Agent} will require a corresponding
   * permission.
   *
   * @param caller the calling {@link Agent}.
   * @param p      the {@link Policy} to be updated.
   * @param a      an {@link Action} instance.
   * @param r      the {link @AuthorizationRule} to be associated with the
   *               given {@link Action}.
   * @return true if updated, false otherwise.
   * @throws SecurityException     if the calling {@link Agent}
   *                               is not authorized or authentic.
   * @throws NoSuchPolicyException if the given {@link Policy}
   *                               is unknown to this <tt>PolicyService</tt>.
   */
  public boolean putPolicyEntry(Agent caller, Policy p, Action a, AuthorizationRule r)
      throws SecurityException, NoSuchPolicyException;

  /**
   * Removes an existing policy entry associated with a specific
   * {@link Action} instance.
   * <p/>
   * The calling {@link Agent} will require a corresponding
   * permissions.
   *
   * @param caller the calling {@link Agent}.
   * @param p      the {@link Policy} to be updated.
   * @param a      an {@link Action} instance.
   * @return true if removed, false otherwise.
   * @throws SecurityException     if the calling {@link Agent}
   *                               is not authorized or authentic.
   * @throws NoSuchActionException if the given {@link Action}
   *                               is unknown to this <tt>PolicyService</tt>.
   * @throws NoSuchPolicyException if the given {@link Policy}
   *                               is unknown to this <tt>PolicyService</tt>.
   */
  public boolean removePolicyEntry(Agent caller, Policy p, Action a)
      throws SecurityException, NoSuchPolicyException, NoSuchActionException;

}//interface PolicyService
