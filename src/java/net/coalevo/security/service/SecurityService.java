/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.service;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.security.model.AuthenticationException;
import net.coalevo.security.model.Authorizations;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.security.model.Role;

import java.util.Set;

/**
 * Provides authentication and authorization functionality
 * for {@link Agent} instances, including
 * {@link net.coalevo.foundation.model.Service}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface SecurityService
    extends Service {

  /**
   * Authenticates a given set of id and cleartext credential,
   * returning a corresponding {@link Agent} instance.
   * <p/>
   * This method should only be used by services doing local
   * (In-VM) authentication. For network authentication,
   * {@link #getServerSRP(String)} should be used.
   * </p>
   *
   * @param aid        the {@link AgentIdentifier}.
   * @param credential a <tt>String</tt>.
   * @return an authenticated {@link Agent} instance.
   * @throws AuthenticationException if authentication is unsuccessful.
   */
  public UserAgent authenticate(AgentIdentifier aid, String credential)
      throws AuthenticationException;

  /**
   * Authenticates a given set of id and credential,
   * returning a corresponding {@link Agent} instance.
   * <p/>
   * This method should only be used by services doing local
   * (In-VM) authentication. For network authentication,
   * {@link #getServerSRP(String)} should be used.
   * </p>
   * This method will authenticate only, it will not prepare any
   * authorization resources for the corresponding agent.
   *
   * @param id         an identifier as <tt>String</tt>.
   * @param credential a <tt>String</tt>.
   * @return a set of strings representing the roles of the
   *          authenticated user.
   * @throws AuthenticationException if authentication is unsuccessful.
   */
  public Set<String> authenticate(String id, String credential)
      throws AuthenticationException;

  /**
   * Returns a {@link ServerSRP} instance that can be
   * used to authenticate clients over the network.
   *
   * @param id the local agent name.
   * @return a {@link ServerSRP} instance for authentication
   *         negotiation with a client.
   */
  public ServerSRP getServerSRP(String id);

  /**
   * Returns the {@link UserAgent} for a {@link ServerSRP}
   * instance that was obtained from {@link #getServerSRP(String)}
   * and subsequently authenticated.
   *
   * @param srp a {@link ServerSRP} instance.
   * @return {@link UserAgent} instance.
   * @throws SecurityException if the given {@link ServerSRP} was
   *                           not obtained from this service or if it was not authenticated.
   */
  public UserAgent getUserAgent(ServerSRP srp) throws SecurityException;

  /**
   * Authenticates a specified {@link Service} instance.
   *
   * @param s a {@link Service} instance.
   * @return a {@link ServiceAgent} instance that should be kept private.
   * @throws AuthenticationException if a service with this identifier was
   *                                 already authenticated, or such a service is not registered.
   */
  public ServiceAgent authenticate(Service s) throws AuthenticationException;

  /**
   * Invalidates a formerly established authenticity of an
   * {@link Agent}.
   *
   * @param a an {@link Agent} instance.
   * @return true if invalidated, false if not an authenticated
   *         agent.
   */
  public boolean invalidateAuthentication(Agent a);

  /**
   * Tests whether the specified {@link Agent} is authentic.
   *
   * @param a an {@link Agent} instance.
   * @return true if authentic, false otherwise.
   */
  public boolean isAuthentic(Agent a);

  /**
   * Tests whether the specified {@link Agent} is authentic.
   *
   * @param a an {@link Agent} instance.
   * @throws SecurityException if the given {@link Agent} is not authentic.
   */
  public void checkAuthentic(Agent a) throws SecurityException;

  /**
   * Returns the {@link Authorizations} associated with the
   * specified {@link Agent}.
   *
   * @param a an {@link Agent} instance.
   * @throws SecurityException if the given {@link Agent} is not authentic.
   * @return the agent's {@link Authorizations}.
   */
  public Authorizations getAuthorizations(Agent a)
      throws SecurityException;

  /**
   * Returns an unmodifiable <tt>Set</tt> all
   * roles granted to the specified agent.
   * <p/>
   * This roles are for information only, it is not recommended
   * to use these for authorization purposes.
   * </p>
   *
   * @param caller an {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @return an unmodifiable <tt>Set</tt> of <tt>String</tt> instances representing role names.
   * @throws SecurityException if the caller is not authentic or not authorized for
   * @throws NoSuchAgentException if the specified agent does not exist.
   */
  public Set<Role> getAgentRoles(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException;

  /**
   * Returns an unmodifiable <tt>Set</tt> with the names of all
   * roles granted to the specified agent.
   * <p/>
   * This role names are for information only, it is not recommended
   * to use these for authorization purposes.
   * </p>
   *
   * @param caller an {@link Agent}.
   * @param aid    an {@link AgentIdentifier}.
   * @return an unmodifiable <tt>Set</tt> of <tt>String</tt> instances representing role names.
   * @throws SecurityException if the caller is not authentic or not authorized for
   * @throws NoSuchAgentException if the specified agent does not exist.
   */
  public Set<String> getAgentRolesByName(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException;

}//interface SecurityService

