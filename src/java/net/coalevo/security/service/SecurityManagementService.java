/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.service;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;
import net.coalevo.security.model.*;

import java.util.Set;

/**
 * Provides functionality for the management of authentication
 * and authorization related data.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface SecurityManagementService
    extends Service {

  /**
   * Returns an {@link Agent} for a given identifier.
   * The agent is probably authentic and authorized, so be
   * careful with the policy for this method.
   *
   * @param caller an {@link Agent}.
   * @param aid     the {@link Agent} identifier as <tt>String</tt>.
   * @return an {@link Agent} instance.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   * @throws NoSuchAgentException if the specified agent does not exist.
   */
  public Agent getAgent(Agent caller, String aid)
      throws SecurityException, NoSuchAgentException;

  /**
   * Returns an unmodifiable <tt>Set</tt>  of {@link Agent} instances
   * registered with this <tt>SecurityManagementService</tt>.
   *
   * @param caller an {@link Agent}.
   * @return an unmodifiable  <tt>Set</tt> of {@link Agent} instances.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Set<Agent> getAgents(Agent caller)
      throws SecurityException;

  /**
   * Register an {@link Agent} with this <tt>SecurityManagementService</tt>.
   *
   * @param caller an {@link Agent}.
   * @param aid    an {@link AgentIdentifier} instance.
   * @param user   true if register a user agent, false otherwise.
   * @return the corresponding {@link Agent} instance, or null otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Agent registerAgent(Agent caller, AgentIdentifier aid, boolean user)
      throws SecurityException;

  /**
   * Unregister an {@link Agent} with a given identifier.
   * <p/>
   * Note that you should not call this lightly, implementations
   * will remove all data stored about this agent automatically.
   *
   * @param caller an {@link Agent}.
   * @param aid    an {@link AgentIdentifier} instance.
   * @return true if unregistered, false otherwise.
   * @throws SecurityException    if the caller is not authentic or not authorized for
   *                              the corresponding action.
   * @throws NoSuchAgentException if the given {@link Role} is unkown to this
   *                              <tt>SecurityManagementService</tt>.
   */
  public boolean unregisterAgent(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException;


  /**
   * Returns an unmodifiable <tt>Set</tt> of all agent identifiers that
   * have been granted a given {@link Role}.
   *
   * @param caller an {@link Agent}.
   * @param r      a {@link Role} instance.
   * @return an unmodifiable <tt>Set</tt> of {@link Agent} instances.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public Set<Agent> getAgentsWithRole(Agent caller, Role r)
      throws SecurityException, NoSuchRoleException;

  /**
   * Creates an authentication for a given {@link Agent}.
   *
   * @param caller     an {@link Agent}.
   * @param a          an {@link Agent}.
   * @param credential a <tt>String</tt> representing the credential.
   * @return true if successful, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public boolean createAuthentication(Agent caller, Agent a, String credential)
      throws SecurityException;

  /**
   * Updates an existing authentication for a given {@link Agent}.
   *
   * @param caller     an {@link Agent}.
   * @param a          an {@link Agent}.
   * @param credential a <tt>String</tt> representing the credential.
   * @return true if successful, false otherwise.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public boolean updateAuthentication(Agent caller, Agent a, String credential)
      throws SecurityException;

  /**
   * Removes the authentication for a given {@link Agent}.
   *
   * @param caller an {@link Agent}.
   * @param a      an {@link Agent}.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public void removeAuthentication(Agent caller, Agent a)
      throws SecurityException;

  /**
   * Returns a {@link Permission} for the given identifier.
   *
   * @param caller an {@link Agent}.
   * @param id     the {@link Permission} identifier as <tt>String</tt>.
   * @param permit the flag that indicates whether an {@link Permit} or {@link Deny}
   *               should be returned.
   * @return a {@link Permission} instance, a {@link Permit} if <tt>permit</tt> was
   *         given as true, or a {@link Deny} otherwise.
   * @throws SecurityException         if the caller is not authentic or not authorized for
   *                                   the corresponding action
   * @throws NoSuchPermissionException if the given {@link Permission} identifier is
   *                                   unkown to this <tt>SecurityManagementService</tt>.
   */
  public Permission getPermission(Agent caller, String id, boolean permit)
      throws SecurityException, NoSuchPermissionException;

  /**
   * Returns the {@link Permission} for the given {@link Permit} or {@link Deny}.
   * <p/>
   * This is a helper method that will translate any permit or deny into
   * its neutral form.
   * </p>
   *
   * @param p a {@link Permission} instance.
   * @return the corresponding {@link Permit} instance.
   */
  public Permission getPermission(Permission p);

  /**
   * Returns the {@link Permit} for the given {@link Permission}.
   * <p/>
   * This is a helper method for {@link #addRolePermission(Agent,Role,Permission)}.
   * </p>
   *
   * @param p a {@link Permission} instance.
   * @return the corresponding {@link Permit} instance.
   */
  public Permission getPermit(Permission p);

  /**
   * Returns the {@link Deny} for the given {@link Permission}.
   * <p/>
   * This is a helper method for {@link #addRolePermission(Agent,Role,Permission)}.
   * </p>
   *
   * @param p a {@link Permission} instance.
   * @return the corresponding {@link Deny} instance.
   */
  public Permission getDeny(Permission p);

  /**
   * Returns an unmodifiable <tt>Set</tt> holding all
   * {@link Permission} instances known to this
   * <tt>SecurityManagementService</tt>.
   *
   * @param caller an {@link Agent}.
   * @return a <tt>Set</tt> of {@link Permission} instances.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Set<Permission> getPermissions(Agent caller)
      throws SecurityException;

  /**
   * Creates a {@link Permission} with a given identifier.
   *
   * @param caller an {@link Agent}.
   * @param id     the {@link Permission} identifier as <tt>String</tt>.
   * @return a {@link Permission} instance or null if creation failed.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Permission createPermission(Agent caller, String id)
      throws SecurityException;

  /**
   * Destroys a {@link Role} with a given identifier.
   *
   * @param caller an {@link Agent}.
   * @param p      a {@link Permission} instance.
   * @return true if destroyed successfully, false otherwise.
   * @throws SecurityException         if the caller is not authentic or not authorized for
   *                                   the corresponding action.
   * @throws NoSuchPermissionException if the given {@link Permission} is unkown to this
   *                                   <tt>SecurityManagementService</tt>.
   */
  public boolean destroyPermission(Agent caller, Permission p)
      throws SecurityException, NoSuchPermissionException;

  /**
   * Returns a {@link Role} for a given identifier.
   *
   * @param caller an {@link Agent}.
   * @param id     the {@link Role} identifier as <tt>String</tt>.
   * @return a {@link Role} instance.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public Role getRole(Agent caller, String id)
      throws SecurityException, NoSuchRoleException;

  /**
   * Creates a {@link Role} with a given identifier.
   *
   * @param caller an {@link Agent}.
   * @param id     the {@link Role} identifier as <tt>String</tt>.
   * @return a {@link Role} instance.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Role createRole(Agent caller, String id)
      throws SecurityException;

  /**
   * Renames a {@link Role}.
   *
   * @param caller an {@link Agent}.
   * @param r      a {@link Role} instance.
   * @param newid  the new identifier as <tt>String</tt>.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public void renameRole(Agent caller, Role r, String newid)
      throws SecurityException, NoSuchRoleException;

  /**
   * Destroys a {@link Role} with a given identifier.
   *
   * @param caller an {@link Agent}.
   * @param r      a {@link Role} instance.
   * @return true if destroyed, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean destroyRole(Agent caller, Role r)
      throws SecurityException, NoSuchRoleException;

  /**
   * Grant a given {@link Role} to a given {@link Agent}.
   * UPR's cannot be granted; they have the same identifier
   * as the actual {@link Agent} they are privately associated
   * with.
   *
   * @param caller an {@link Agent}.
   * @param a      the {@link Agent} the {@link Role} will be granted to.
   * @param r      a {@link Role} instance.
   * @return true if granted, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean grantRole(Agent caller, Agent a, Role r)
      throws SecurityException, NoSuchRoleException;

  /**
   * Revoke a given {@link Role} from a given {@link Agent}.
   *
   * @param caller an {@link Agent}.
   * @param a      the {@link Agent} the {@link Role} will revoked from.
   * @param r      a {@link Role} instance.
   * @return true if revoked, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean revokeRole(Agent caller, Agent a, Role r)
      throws SecurityException, NoSuchRoleException;

  /**
   * Returns an unmodifiable <tt>Set</tt> holding all
   * {@link Role} instances that have been granted to the given
   * {@link Agent}.
   *
   * @param caller an {@link Agent}.
   * @param a      an {@link Agent}.
   * @return an unmodifiable <tt>Set</tt> of {@link Role} instances.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Set<Role> getAgentRoles(Agent caller, Agent a)
      throws SecurityException;

  /**
   * Returns an unmodifiable <tt>Set</tt> holding all
   * {@link Role} instances known to this
   * <tt>SecurityManagementService</tt>.
   *
   * @param caller an {@link Agent}.
   * @return an unmodifiable <tt>Set</tt> of {@link Role} instances.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Set<Role> getRoles(Agent caller)
      throws SecurityException;

  /**
   * Returns the {@link Set} of {@link Permission} instances associated
   * with the given {@link Role}.
   *
   * @param caller an {@link Agent}.
   * @param r      a {@link Role}.
   * @return an unmodifiable {@link Set} instance.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   * @throws SecurityException if the caller is not authentic or not authorized for
   *                           the corresponding action.
   */
  public Set<Permission> getRolePermissions(Agent caller, Role r)
      throws SecurityException, NoSuchRoleException;

  /**
   * Adds a {@link Permission} for a given {@link Role}.
   * </p>
   * Authorization will be required for the calling {@link Agent}.
   *
   * @param caller an {@link Agent}.
   * @param r      the {@link Role} the {@link Permission} applies to.
   * @param p      a {@link Permission} instance.
   * @return true if added, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean addRolePermission(Agent caller, Role r, Permission p)
      throws SecurityException, NoSuchRoleException;

  /**
   * Adds a <tt>Set</tt> of {@link Permission} instances to a
   * given {@link Role}.
   * </p>
   * Authorization will be required for the calling {@link Agent}.
   * Note that you should translate the {@link Permission} instances
   * to either {@link Permit} or {@link Deny} instances, using
   * {@link #getPermit(Permission)} and {@link #getDeny(Permission)}.
   *
   * @param caller an {@link Agent}.
   * @param r      the {@link Role} the {@link Permission} instances will be added to.
   * @param s      a <tt>Set</tt> of {@link Permission} instances.
   * @return true if added, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean addRolePermissions(Agent caller, Role r, Set s)
      throws SecurityException, NoSuchRoleException;

  /**
   * Removes a {@link Permission} from a given {@link Role}.
   * </p>
   * Authorization will be required for the calling {@link Agent}.
   *
   * @param caller a {@link Agent}.
   * @param r      the {@link Role} the {@link Permission} will be removed from.
   * @param p      a {@link Permission} instance.
   * @return true if added, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean removeRolePermission(Agent caller, Role r, Permission p)
      throws SecurityException, NoSuchRoleException;

  /**
   * Removes a <tt>Set</tt> of {@link Permission} instance
   * from a given {@link Role}.
   * </p>
   * Authorization will be required for the calling {@link Agent}.
   *
   * @param caller a {@link Agent}.
   * @param r      the {@link Role} the {@link Permission} will be removed from.
   * @param s      a <tt>Set</tt> of {@link Permission} instances.
   * @return true if added, false otherwise.
   * @throws SecurityException   if the caller is not authentic or not authorized for
   *                             the corresponding action.
   * @throws NoSuchRoleException if the given {@link Role} is unkown to this
   *                             <tt>SecurityManagementService</tt>.
   */
  public boolean removeRolePermissions(Agent caller, Role r, Set s)
      throws SecurityException, NoSuchRoleException;

}//interface SecurityManagementService
