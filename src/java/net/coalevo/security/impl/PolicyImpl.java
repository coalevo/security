/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.security.model.*;
import net.coalevo.security.service.PolicyService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Provides an implementation of {@link Policy}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PolicyImpl
    implements Policy {

  private final String m_Identifier; //non modifiable
  protected PolicyService m_PolicyService;
  protected Map<Action,AuthorizationRule> m_AuthRules;
  protected int m_RefCount = 0;

  public PolicyImpl(PolicyService ps, String id) {
    m_PolicyService = ps;
    m_Identifier = id;
    m_AuthRules = new HashMap<Action,AuthorizationRule>();
  }//PolicyImpl

  public PolicyImpl(PolicyService ps, String id, Map<Action,AuthorizationRule> policy) {
    m_PolicyService = ps;
    m_Identifier = id;
    m_AuthRules = policy;
  }//constructor

  public PolicyService getPolicyService() {
    return m_PolicyService;
  }//getPolicyService

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public boolean checkAuthorization(Authorizations auth, Action a)
      throws NoSuchActionException {
    Object o = m_AuthRules.get(a);
    if (o == null) {
      throw new NoSuchActionException(a);
    } else {
      return ((AuthorizationRule) o).isAuthorized(auth);
    }
  }//checkAutorization

  public void ensureAuthorization(Authorizations auth, Action a)
      throws AuthorizationException, NoSuchActionException {
    if (!checkAuthorization(auth, a)) {
      throw new AuthorizationException(a);
    }
  }//ensureAuthorization

  public boolean putEntry(Action a, AuthorizationRule r) {
    return m_AuthRules.put(a, r) != null;
  }//putEntry

  public boolean removeEntry(Action a) {
    return m_AuthRules.remove(a) != null;
  }//removeEntry

  public boolean hasEntry(Action a) {
    return m_AuthRules.containsKey(a);
  }//hasEntry

  public Set getActions() {
    return Collections.unmodifiableSet(m_AuthRules.keySet());
  }//getActions

  public AuthorizationRule getRuleFor(Action a) {
    Object o = m_AuthRules.get(a);
    if (o != null) {
      return (AuthorizationRuleImpl) o;
    } else {
      return null;
    }
  }//getRuleFor

  public String toString() {
    return m_Identifier;
  }//toString

  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof PolicyImpl)) return false;

    final PolicyImpl policy = (PolicyImpl) o;

    if (m_Identifier != null ? !m_Identifier.equals(policy.m_Identifier) : policy.m_Identifier != null) return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

  public synchronized void incrementRefCount() {
    m_RefCount++;
  }//incremetRefCount

  public synchronized void decrementRefCount() {
    m_RefCount--;
  }//decrementRefCount

  public boolean isReferenced() {
    return (m_RefCount > 0);
  }//isReferenced


}//class PolicyImpl
