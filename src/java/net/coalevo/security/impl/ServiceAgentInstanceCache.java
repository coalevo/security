/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.security.model.NoSuchAgentException;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a package-local cache for {@link ServiceAgentImpl} instances,
 * capable of creating them when required.
 * <p/>
 * The actual cache is based on an LRU CacheMap of size 25.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceAgentInstanceCache
    extends BaseIdentifiable {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceAgentInstanceCache.class.getName());
  private SecurityStore m_Store;
  private LRUCacheMap<String, ServiceAgent> m_Cache;

  private ServiceAgentInstanceCache(SecurityStore store) {
    this(store, 25);
  }//private constructor

  private ServiceAgentInstanceCache(SecurityStore store, int size) {
    super(ServiceAgentInstanceCache.class.getName());
    m_Store = store;
    m_Cache = new LRUCacheMap<String,ServiceAgent>(size);
  }//private constructor


  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  private ServiceAgentImpl createInstance(String str)
      throws NoSuchAgentException {
    ResultSet rs = null;
    Map<String,String> params = new HashMap<String,String>();
    params.put(AGENT_ID, str);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getServiceAgent", params);
      if (rs.next()) {
        return new ServiceAgentImpl(str);
      } else {
        throw new NoSuchAgentException(str);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createInstance()::", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//createInstance

  public ServiceAgentImpl getAgent(String id)
      throws NoSuchAgentException {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(id);
    }
    if (o != null) {
      return (ServiceAgentImpl) o;
    } else {
      ServiceAgentImpl r = createInstance(id);
      synchronized (m_Cache) {
        m_Cache.put(id, r);
      }
      return r;
    }
  }//getAgent

  public boolean contains(String id) {
    return m_Cache.containsKey(id);
  }//contains

  public void invalidate(Agent a) {
    synchronized (m_Cache) {
      m_Cache.remove(a.getIdentifier());
    }
  }//invalidate

  public void invalidate(String id) {
    synchronized (m_Cache) {
      m_Cache.remove(id);
    }
  }//invalidate


  public void clear() {
    synchronized (m_Cache) {
      m_Cache.clear(false);
    }
  }//clear

  public String toString() {
    return m_Cache.toString();
  }//toString


  public static ServiceAgentInstanceCache createAgentCache(SecurityStore st) {
    return new ServiceAgentInstanceCache(st);
  }//createRolePermissionsCache

  public static ServiceAgentInstanceCache createAgentCache(SecurityStore st, int size) {
    return new ServiceAgentInstanceCache(st, size);
  }//createRolePermissionsCache

  private static final String AGENT_ID = "agent_id";

}//class RoleInstanceCache
