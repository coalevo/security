// $ANTLR 2.7.5 (20050128): "sarl.g" -> "AuthorizationRuleImpl.java"$

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import antlr.NoViableAltException;
import antlr.RecognitionException;
import antlr.collections.AST;
import net.coalevo.security.model.AuthorizationRule;
import net.coalevo.security.model.Authorizations;

import java.io.StringReader;


/**
 * Provides a <tt>TreeParser</tt> for SARL that
 * was automatically generated from a grammar
 * by ANTLR.
 *
 * @author Dieter Wimberger (grammar)
 * @author ANTLR
 */
class AuthorizationRuleImpl extends antlr.TreeParser implements AuthorizationRuleLexerTokenTypes
    , AuthorizationRule {


  protected String m_Rule;
  protected AST m_RuleTree;
  protected Authorizations m_Authorizations;

  public String getRule() {
    return m_Rule;
  }//getRule

  public synchronized boolean isAuthorized(Authorizations a) {
    boolean b = false;
    m_Authorizations = a;
    try {
      b = expr(m_RuleTree);
    } catch (RecognitionException ex) {
    } finally {
      //clear agent reference
      m_Authorizations = null;
    }
    return b;
  }//isAuthorized

  public static AuthorizationRule create(String rule) throws Exception {
    //1. Lexer
    AuthorizationRuleLexer lexer = new AuthorizationRuleLexer(new StringReader(rule));
    //2. Parser
    AuthorizationRuleParser parser = new AuthorizationRuleParser(lexer);
    parser.expr();
    AuthorizationRuleImpl ari = new AuthorizationRuleImpl();
    ari.m_RuleTree = parser.getAST();
    ari.m_Rule = rule;
    return ari;
  }//create

  public String toString() {
    return m_Rule;
  }//getRule

  public String toSARL() {
    return m_Rule;
  }//toSARL

  public AuthorizationRuleImpl() {
    tokenNames = _tokenNames;
  }

  protected final boolean expr(AST _t) throws RecognitionException {
    boolean r;

    AST expr_AST_in = (_t == ASTNULL) ? null : (AST) _t;
    AST perm = null;
    AST role = null;

    boolean a, b;
    r = false;


    try {      // for error handling
      if (_t == null) _t = ASTNULL;
      switch (_t.getType()) {
        case AND: {
          AST __t18 = _t;
          AST tmp8_AST_in = (AST) _t;
          match(_t, AND);
          _t = _t.getFirstChild();
          a = expr(_t);
          _t = _retTree;
          b = expr(_t);
          _t = _retTree;
          _t = __t18;
          _t = _t.getNextSibling();
          r = a && b;
          break;
        }
        case OR: {
          AST __t19 = _t;
          AST tmp9_AST_in = (AST) _t;
          match(_t, OR);
          _t = _t.getFirstChild();
          a = expr(_t);
          _t = _retTree;
          b = expr(_t);
          _t = _retTree;
          _t = __t19;
          _t = _t.getNextSibling();
          r = a || b;
          break;
        }
        case PERMISSION: {
          perm = (AST) _t;
          match(_t, PERMISSION);
          _t = _t.getNextSibling();
          r = m_Authorizations.hasPermission(perm.getText());
          break;
        }
        case ROLE: {
          role = (AST) _t;
          match(_t, ROLE);
          _t = _t.getNextSibling();
          r = m_Authorizations.hasRole(role.getText().substring(1));
          break;
        }
        default: {
          throw new NoViableAltException(_t);
        }
      }
    }
    catch (RecognitionException ex) {
      reportError(ex);
      if (_t != null) {
        _t = _t.getNextSibling();
      }
    }
    _retTree = _t;
    return r;
  }


  public static final String[] _tokenNames = {
      "<0>",
      "EOF",
      "<2>",
      "NULL_TREE_LOOKAHEAD",
      "PERMISSION",
      "ROLE",
      "AND",
      "OR",
      "LPAREN",
      "RPAREN",
      "SEMI",
      "WS"
  };

}
	
