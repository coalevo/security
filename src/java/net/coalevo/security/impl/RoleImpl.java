/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.security.model.Role;

/**
 * Provides a simple implementation of {@link Role}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RoleImpl
    extends BaseIdentifiable
    implements Role {

  private final int m_Number;

  public RoleImpl(int num, String id) {
    super(id);
    m_Number = num;
  }//constructor

  public int getNumber() {
    return m_Number;
  }//getNumber

}//class RoleImpl
