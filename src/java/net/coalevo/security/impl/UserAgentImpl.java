/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Identifiable;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.UserAgent;

/**
 * Provides a {@link UserAgent} implementation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserAgentImpl
    implements Identifiable, UserAgent {

  private Session m_Session;
  protected final String m_Identifier;
  protected final AgentIdentifier m_AgentIdentifier;

  public UserAgentImpl(String id) {
    m_Identifier = id;
    m_AgentIdentifier = new AgentIdentifier(id);
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

  public Session getSession() {
    return m_Session;
  }//getSession

  public void setSession(Session s) {
    if (m_Session != null) {
      m_Session.invalidate();
    }
    m_Session = s;
  }//setSession

  public String toString() {
    return m_Identifier;
  }//toString

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    final UserAgentImpl userAgent = (UserAgentImpl) o;

    if (m_Identifier != null ? !m_Identifier.equals(userAgent.m_Identifier) : userAgent.m_Identifier != null)
      return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

}//class UserAgentImpl
