/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.security.model.NoSuchRoleException;
import net.coalevo.security.model.Role;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a package local cache for permissions
 * associated with a specific {@link Role}.
 * <p/>
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RolePermissionsCache extends BaseIdentifiable {

  private Marker m_LogMarker = MarkerFactory.getMarker(RolePermissionsCache.class.getName());
  private SecurityStore m_Store;
  private LRUCacheMap<String,PermissionSet> m_Cache;

  private RolePermissionsCache(SecurityStore store) {
    this(store, 25);
  }//private constructor

  private RolePermissionsCache(SecurityStore store, int size) {
    super(RolePermissionsCache.class.getName());
    m_Store = store;
    m_Cache = new LRUCacheMap<String,PermissionSet>(size);
  }//private constructor

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  private PermissionSet createInstance(RoleImpl r) {
    Activator.log().debug(m_LogMarker,"Loading Permissions for role " + r.getIdentifier());
    Map<String,String> params = new HashMap<String,String>();
    params.put("role_id", "" + r.getNumber());
    ResultSet rs = null;
    PermissionSet set = new PermissionSet(m_Store.getPermitCache(), m_Store.getDenyCache());
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listRolePermissions", params);
      while (rs.next()) {
        set.add(rs.getInt(1), rs.getString(2), rs.getInt(3) == 1);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createInstance()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return set;
  }//createInstance

  public PermissionSet getPermissions(RoleImpl r)
      throws NoSuchRoleException {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(r.getIdentifier());
    }
    if (o != null) {
      return (PermissionSet) o;
    } else {
      PermissionSet ps = createInstance(r);
      synchronized (m_Cache) {
        m_Cache.put(r.getIdentifier(), ps);
      }
      return ps;
    }
  }//getPermissions

  public void invalidate(Role r) {
    synchronized (m_Cache) {
      m_Cache.remove(r.getIdentifier());
    }
  }//invalidate

  public void invalidateAll() {
    synchronized (m_Cache) {
      m_Cache.clear(false);
    }
  }//invalidateAll

  public void clear() {
    invalidateAll();
  }//clear

  public String toString() {
    return m_Cache.toString();
  }//toString


  public static RolePermissionsCache createRolePermissionsCache(SecurityStore st) {
    return new RolePermissionsCache(st);
  }//createRolePermissions

  public static RolePermissionsCache createRolePermissionsCache(SecurityStore st, int size) {
    return new RolePermissionsCache(st, size);
  }//createRolePermissions

}//class RolePermissionsCache
