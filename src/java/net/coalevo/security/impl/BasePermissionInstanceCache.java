/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.security.model.NoSuchPermissionException;
import net.coalevo.security.model.Permission;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides the base instance cache mechanism for {@link Permission}
 * instances of any implementation type.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
abstract class BasePermissionInstanceCache
    extends BaseIdentifiable {

  protected Marker m_LogMarker = MarkerFactory.getMarker(BasePermissionInstanceCache.class.getName());

  protected LRUCacheMap<String,Permission> m_Cache;
  protected SecurityStore m_Store;

  protected BasePermissionInstanceCache(SecurityStore store) {
    this(store, 50);
  }//private constructor

  protected BasePermissionInstanceCache(SecurityStore store, int size) {
    super(PermissionInstanceCache.class.getName());
    m_Store = store;
    m_Cache = new LRUCacheMap<String,Permission>(size);
  }//private constructor

  /**
   * Sets maximum number of cached instances.
   * <p/>
   * The size can be adjusted at runtime.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  protected abstract Permission createInstance(String str)
      throws NoSuchPermissionException;

  protected abstract Permission createInstance(int num, String str)
      throws NoSuchPermissionException;


  private Object getFromCache(String id) {
    synchronized (m_Cache) {
      return m_Cache.get(id);
    }
  }//getFromCache

  private Permission putIntoCache(String id, Permission p) {
    synchronized (m_Cache) {
      return m_Cache.put(id, p);
    }
  }//putIntoCache

  private Permission removeFromCache(Permission p) {
    synchronized (m_Cache) {
      return m_Cache.remove(p.getIdentifier());
    }
  }//getFromCache

  public Permission getPermission(String id)
      throws NoSuchPermissionException {
    Object o = getFromCache(id);
    if (o != null) {
      return (Permission) o;
    } else {
      Permission p = createInstance(id);
      putIntoCache(id, p);
      return p;
    }
  }//getPermission

  public Permission getPermission(int num, String id) {
    Object o = getFromCache(id);
    if (o != null) {
      return (Permission) o;
    } else {
      Permission p = createInstance(num, id);
      putIntoCache(id, p);
      return p;
    }
  }//getPermission

  public void invalidate(Permission p) {
    removeFromCache(p);
  }//invalidate

  public void clear() {
    synchronized (m_Cache) {
      m_Cache.clear(false);
    }
  }//clear

  public static final String PERMISSION_NAME = "permname";

}//class BasePermissionInstanceCache
