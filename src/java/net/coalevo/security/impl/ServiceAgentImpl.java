/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Service;
import net.coalevo.foundation.model.ServiceAgent;

/**
 * Provides an implementation of {@link Agent} for
 * {@link Service} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceAgentImpl implements ServiceAgent {

  private String m_Identifier;
  private AgentIdentifier m_AgentIdentifier;

  public ServiceAgentImpl(String id) {
    m_Identifier = id;
    m_AgentIdentifier = new AgentIdentifier(id);
  }//constructor

  public ServiceAgentImpl(Service s) {
    this(s.getIdentifier());
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

}//class ServiceAgentImpl
