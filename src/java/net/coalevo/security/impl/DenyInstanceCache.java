/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.security.model.NoSuchPermissionException;
import net.coalevo.security.model.Permission;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a package-local cache for {@link DenyImpl}
 * instances, creating instances from the database information when
 * required.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class DenyInstanceCache
    extends BasePermissionInstanceCache {

  public DenyInstanceCache(SecurityStore store, int cachesize) {
    super(store, cachesize);
  }//constructor

  protected Permission createInstance(String str)
      throws NoSuchPermissionException {
    ResultSet rs = null;
    Map<String,String> params = new HashMap<String,String>();
    params.put(PERMISSION_NAME, str);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getPermissionNumber", params);
      if (rs.next()) {
        return new DenyImpl(rs.getInt(1), str);
      } else {
        throw new NoSuchPermissionException(str);
      }
    } catch (Exception ex) {
      Activator.log().error("createInstance()::", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//createInstance

  protected Permission createInstance(int id, String str)
      throws NoSuchPermissionException {
    return new DenyImpl(id, str);
  }//createInstance

}//class DenyInstanceCache
