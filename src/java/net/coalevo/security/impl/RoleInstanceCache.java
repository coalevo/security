/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.BaseIdentifiable;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.security.model.NoSuchRoleException;
import net.coalevo.security.model.Role;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides a package-local cache for roles, capable of
 * creating them when required.
 * <p/>
 * The actual cache is based on an LRU CacheMap of size 25.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RoleInstanceCache extends BaseIdentifiable {

  private Marker m_LogMarker = MarkerFactory.getMarker(RoleInstanceCache.class.getName());
  private SecurityStore m_Store;
  private LRUCacheMap<String,Role> m_Cache;

  private RoleInstanceCache(SecurityStore store) {
    this(store, 50);
  }//private constructor

  private RoleInstanceCache(SecurityStore store, int size) {
    super(RoleInstanceCache.class.getName());
    m_Store = store;
    m_Cache = new LRUCacheMap<String,Role>(size);
  }//private constructor

  /**
   * Returns the maximum number of cached instances.
   *
   * @return the maximum number of cached instances.
   */
  public int getCacheSize() {
    return m_Cache.getCeiling();
  }//getCacheSize

  /**
   * Sets maximum number of cached instances.
   *
   * @param size the maximum cached instances.
   */
  public void setCacheSize(int size) {
    synchronized (m_Cache) {
      m_Cache.setCeiling(size);
    }
  }//setCacheSize

  private RoleImpl createInstance(String str)
      throws NoSuchRoleException {
    ResultSet rs = null;
    Map<String,String> params = new HashMap<String,String>();
    params.put(ROLE_NAME, str);
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getRoleNumber", params);
      if (rs.next()) {
        return new RoleImpl(rs.getInt(1), str);
      } else {
        throw new NoSuchRoleException(str);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createInstance()::", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
  }//createInstance

  public RoleImpl getRole(String id)
      throws NoSuchRoleException {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(id);
    }
    if (o != null) {
      return (RoleImpl) o;
    } else {
      RoleImpl r = createInstance(id);
      synchronized (m_Cache) {
        m_Cache.put(id, r);
      }
      return r;
    }
  }//getRole

  public RoleImpl getRole(int num, String id) {
    Object o = null;
    synchronized (m_Cache) {
      o = m_Cache.get(id);
    }
    if (o != null) {
      return (RoleImpl) o;
    } else {
      RoleImpl r = new RoleImpl(num, id);
      synchronized (m_Cache) {
        m_Cache.put(id, r);
      }
      return r;
    }
  }//getRole

  public void invalidate(Role r) {
    synchronized (m_Cache) {
      m_Cache.remove(r.getIdentifier());
    }
  }//invalidate

  public void clear() {
    synchronized (m_Cache) {
      m_Cache.clear(false);
    }
  }//clear

  public static RoleInstanceCache createRoleCache(SecurityStore st) {
    return new RoleInstanceCache(st);
  }//createRolePermissionsCache

  public static RoleInstanceCache createRoleCache(SecurityStore st, int size) {
    return new RoleInstanceCache(st, size);
  }//createRolePermissionsCache

  public String toString() {
    return m_Cache.toString();
  }//toString

  private static final String ROLE_NAME = "rolename";

}//class RoleInstanceCache
