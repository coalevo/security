/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import net.coalevo.security.service.SecurityService;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.xml.parsers.SAXParserFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;
  private ConfigurationMediator m_ConfigMediator;

  private MessageResourceService m_MessageResourceService;
  private RndStringGeneratorService m_RndStringGeneratorService;
  private DataSourceService m_DataSourceService;
  private SAXParserFactory m_SAXParserFactory;

  private CountDownLatch m_RndStringGeneratorLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_DataSourceServiceLatch;
  private CountDownLatch m_SAXParserFactoryLatch;

  //local services
  private SecurityService m_SecurityService;
  private PolicyService m_PolicyService;
  private PolicyXMLService m_PolicyXMLService;

  public ServiceMediator() {

  }//constructor

  public SAXParserFactory getSAXParserFactory(long wait) {
    try {
      if (wait < 0) {
        m_SAXParserFactoryLatch.await();
      } else if (wait > 0) {
        m_SAXParserFactoryLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getSAXParserFactory()", e);
    }
    return m_SAXParserFactory;
  }//getSAXParserFactory

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public RndStringGeneratorService getRndStringGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_RndStringGeneratorLatch.await();
      } else if (wait > 0) {
        m_RndStringGeneratorLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getRndStringGeneratorService()", e);
    }
    return m_RndStringGeneratorService;
  }//getRndStringGeneratorService

  public DataSourceService getDataSourceService(long wait) {
    try {
      if (wait < 0) {
        m_DataSourceServiceLatch.await();
      } else if (wait > 0) {
        m_DataSourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getDataSourceService()", e);
    }
    return m_DataSourceService;
  }//getDataSourceService

  public SecurityService getSecurityService() {
    return m_SecurityService;
  }//getSecurityService

  public void setSecurityService(SecurityService service) {
    m_SecurityService = service;
  }//setSecurityService

  public PolicyService getPolicyService() {
    return m_PolicyService;
  }//getPolicyService

  public void setPolicyService(PolicyService service) {
    m_PolicyService = service;
  }//setPolicyService

  public PolicyXMLService getPolicyXMLService() {
    return m_PolicyXMLService;
  }//getPolicyXMLService

  public void setPolicyXMLService(PolicyXMLService policyXMLService) {
    m_PolicyXMLService = policyXMLService;
  }//setPolicyXMLService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigMediator = configMediator;
  }//setConfigMediator


  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_RndStringGeneratorLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_DataSourceServiceLatch = createWaitLatch();
    m_SAXParserFactoryLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(objectclass=" + RndStringGeneratorService.class.getName() + ")" +
            "(objectclass=" + MessageResourceService.class.getName() + "))" +
            "(objectclass=" + DataSourceService.class.getName() + "))" +
            "(objectclass=" + SAXParserFactory.class.getName() + "))";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException e) {
      Activator.log().error(m_LogMarker, "activate()", e);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MessageResourceService = null;
    m_DataSourceService = null;
    m_SAXParserFactory = null;
    m_RndStringGeneratorService = null;

    if(m_RndStringGeneratorLatch != null) {
      m_RndStringGeneratorLatch.countDown();
      m_RndStringGeneratorLatch = null;
    }
    if(m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if(m_DataSourceServiceLatch != null) {
      m_DataSourceServiceLatch.countDown();
      m_DataSourceServiceLatch = null;
    }
    if(m_SAXParserFactoryLatch != null) {
      m_SAXParserFactoryLatch.countDown();
      m_SAXParserFactoryLatch = null;
    }
    m_SecurityService = null;
    m_PolicyService = null;
    m_PolicyXMLService = null;

    m_LogMarker = null;
    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = (RndStringGeneratorService) o;
            m_RndStringGeneratorLatch.countDown();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof DataSourceService) {
            m_DataSourceService = (DataSourceService) o;
            m_DataSourceServiceLatch.countDown();
          } else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = (SAXParserFactory) o;
            m_SAXParserFactory.setValidating(false);
            m_SAXParserFactory.setNamespaceAware(true);
            m_SAXParserFactoryLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = null;
            m_RndStringGeneratorLatch = createWaitLatch();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof DataSourceService) {
            m_DataSourceService = null;
            m_DataSourceServiceLatch = createWaitLatch();
          } else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = null;
            m_SAXParserFactoryLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
