/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.security.model.Permit;

/**
 * Provides a package local implementation of a
 * {@link Permit}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PermitImpl
    extends PermissionImpl
    implements Permit {

  public PermitImpl(int num, String id) {
    super(num, id);
  }//constructor

  public String toString() {
    return new StringBuilder("Permit [")
        .append(m_Number)
        .append(",")
        .append(m_Identifier)
        .append("]")
        .toString();
  }//toString

}//class PermitImpl
