/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.security.service.*;
import net.coalevo.logging.model.LogProxy;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;


/**
 * This class is the <tt>BundleActivator</tt> implementation
 * for the security bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {


  private static Marker c_LogMarker;
  private static LogProxy c_Log;

  private static Messages c_BundleMessages;
  private static ServiceMediator c_Services;
  private static BundleConfiguration c_BundleConfiguration;

  private SecurityServiceImpl m_SecurityService;
  private PolicyService m_PolicyService;
  private SecurityManagementService m_SecurityManagementService;
  private PolicyXMLService m_PolicyXMLService;
  private SecurityStore m_SecurityStore;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext) throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(new Runnable() {
      public void run() {
        try {
          //1. Log
          c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
          c_Log = new LogProxy();
          c_Log.activate(bundleContext);


          //2. Services
          c_Services = new ServiceMediator();
          c_Services.activate(bundleContext);

          //2. Bundle messages
          c_BundleMessages =
              c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED)
                  .getBundleMessages(bundleContext.getBundle());

          //3. BundleConfiguration
          c_BundleConfiguration = new BundleConfiguration(SecurityConfiguration.class.getName());
          c_BundleConfiguration.activate(bundleContext);
          c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

          //4. Prepare Store
          m_SecurityStore = new SecurityStore();
          m_SecurityStore.activate();

          //5. Prepare Services
          //SecurityService
          m_SecurityService = new SecurityServiceImpl(m_SecurityStore);
          if (!m_SecurityService.activate(bundleContext)) {
            log().error(c_BundleMessages.get("Activator.activation.exception", "service", "SecurityService"));
            return;
          }
          c_Services.setSecurityService(m_SecurityService);

          //PolicyService
          m_PolicyService = new PolicyServiceImpl(m_SecurityStore);
          if (!m_PolicyService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "PolicyService"));
            return;
          }
          c_Services.setPolicyService(m_PolicyService);

          //PolicyXMLService
          m_PolicyXMLService = new PolicyXMLServiceImpl();
          if (!m_PolicyXMLService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "PolicyXMLService"));
            return;
          }
          c_Services.setPolicyXMLService(m_PolicyXMLService);

          //SecurityManagementService
          m_SecurityManagementService = new SecurityManagementServiceImpl(m_SecurityStore);
          if (!m_SecurityManagementService.activate(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.exception", "service", "SecurityManagementService"));
            return;
          }

          //Activate SecurityService Security
          if (!m_SecurityService.activateSecurity(bundleContext)) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.activation.security"));
            return;
          }

          //5. Register externally used service instances
          String[] classes = {SecurityService.class.getName(), Maintainable.class.getName()};
          bundleContext.registerService(
              classes,
              m_SecurityService,
              null);
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "SecurityService"));
          bundleContext.registerService(
              PolicyService.class.getName(),
              m_PolicyService,
              null);
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "PolicyService"));
          bundleContext.registerService(
              PolicyXMLService.class.getName(),
              m_PolicyXMLService,
              null
          );
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "PolicyXMLService"));
          bundleContext.registerService(
              SecurityManagementService.class.getName(),
              m_SecurityManagementService,
              null);
          log().info(c_LogMarker, c_BundleMessages.get("Activator.activation.service", "service", "SecurityManagementService"));


          log().info(c_LogMarker, c_BundleMessages.get("Activator.started"));

        } catch (Exception ex) {
          log().error(c_LogMarker, "start()", ex);
          m_SecurityManagementService = null;
          m_PolicyService = null;
          m_SecurityService = null;
          m_SecurityStore = null;
          c_Services = null;
          c_BundleMessages = null;
          c_BundleConfiguration = null;
        }

      }//run
    }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(this.getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {

    //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    //deactivate registered services
    if (m_SecurityManagementService != null) {
      m_SecurityManagementService.deactivate();
      m_SecurityManagementService = null;
    }
    if (m_PolicyService != null) {
      m_PolicyService.deactivate();
      m_PolicyService = null;
    }
    if (m_SecurityService != null) {
      m_SecurityService.deactivate();
      m_SecurityService = null;
    }
    if (m_SecurityStore != null) {
      m_SecurityStore.deactivate();
      m_SecurityStore = null;
    }
    if (c_BundleConfiguration != null) {
        c_BundleConfiguration.deactivate();
        c_BundleConfiguration = null;
    }
    if (m_PolicyXMLService != null) {
      m_PolicyXMLService.deactivate();
      m_PolicyXMLService = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if(c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }

    c_LogMarker = null;
    c_BundleMessages = null;
  }//stop

  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

}//class Activator