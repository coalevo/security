/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.srp.StoreEntry;
import net.coalevo.security.model.*;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import net.coalevo.security.service.SecurityManagementService;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.util.*;


/**
 * Provides an implementation of {@link SecurityManagementService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SecurityManagementServiceImpl
    extends BaseService
    implements SecurityManagementService {

  private Marker m_LogMarker = MarkerFactory.getMarker(SecurityManagementServiceImpl.class.getName());
  private boolean m_Active = false;
  private ServiceAgent m_ServiceAgent; //never pass out this instance.
  private SecurityServiceImpl m_SecurityService;
  private SecurityStore m_SecurityStore;
  private UserAgentInstanceCache m_UserAgentCache;
  private ServiceAgentInstanceCache m_ServiceAgentCache;
  private RoleInstanceCache m_RoleCache;
  private BasePermissionInstanceCache m_PermissionCache;
  private BasePermissionInstanceCache m_PermitCache;
  private BasePermissionInstanceCache m_DenyCache;

  private RolePermissionsCache m_RolePermissionsCache;
  private Policy m_Policy;
  private ServiceMediator m_Services;
  private Messages m_BundleMessages;

  public SecurityManagementServiceImpl(SecurityStore securityStore) {
    super(SecurityManagementService.class.getName(), ACTIONS);
    m_SecurityStore = securityStore;
    m_RoleCache = m_SecurityStore.getRoleCache();
    m_PermissionCache = m_SecurityStore.getPermissionCache();
    m_PermitCache = m_SecurityStore.getPermitCache();
    m_DenyCache = m_SecurityStore.getDenyCache();
    m_RolePermissionsCache = m_SecurityStore.getRolePermissionsCache();
    m_UserAgentCache = m_SecurityStore.getUserAgentCache();
    m_ServiceAgentCache = m_SecurityStore.getServiceAgentCache();
  }//constructor


  public boolean activate(BundleContext bc) {
    if (m_Active) {
      Activator.log().error(m_LogMarker, "activate()::Already active.");
      return false;
    }
    m_BundleMessages = Activator.getBundleMessages();
    m_Services = Activator.getServices();
    m_SecurityService = (SecurityServiceImpl) m_Services.getSecurityService();
    if (m_SecurityService == null) {
      Activator.log().error(m_BundleMessages.get("SecurityManagementServiceImpl.exceptions.service", "service", "SecurityService"));
      return false;
    }

    //2. Authenticate ourself
    try {
      m_ServiceAgent = m_SecurityService.authenticate(this);
    } catch (AuthenticationException ex) {
      Activator.log().error(m_LogMarker, "activate()::Could not authenticate this SecurityManagementService.", ex);
      return false;
    }
    //3. Obtain Policy
    PolicyService policyService = m_Services.getPolicyService();
    if (policyService == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("SecurityManagementServiceImpl.exceptions.service", "service", "PolicyService"));
      return false;
    }
    //ensure basic security policies
    if (policyService.isPolicyAvailable(getIdentifier())) {
      m_Policy = policyService.leasePolicy(m_ServiceAgent, getIdentifier());
    } else {
      try {
        PolicyXMLService policyXMLService = m_Services.getPolicyXMLService();
        if (policyService == null) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("error.reference", "service", "PolicyXMLService"));
        }
        //load from embedded file
        URL policy = bc.getBundle().getEntry("COALEVO-INF/security/SecurityManagementService-policy.xml");
        Policy p = policyXMLService.fromXML(new InputStreamReader(policy.openStream()));
        //store it
        for (Iterator iterator = p.getActions().iterator(); iterator.hasNext();) {
          Action a = (Action) iterator.next();
          m_Policy = policyService.createPolicy(m_ServiceAgent, getIdentifier());
          boolean b = policyService.putPolicyEntry(m_ServiceAgent, m_Policy, a, p.getRuleFor(a));
          if (!b) {
            Activator.log().error(m_LogMarker, m_BundleMessages.get("error.policyrule", "action", a.getIdentifier()));
          }
        }
        Activator.log().info(m_LogMarker, m_BundleMessages.get(
            "SecurityManagementServiceImpl.activation.policy",
            "service", "SecurityManagementService",
            "file", policy.toString())
        );
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "activate()", ex);
      }
    }

    m_Active = true;
    return true;
  }//activate

  public boolean deactivate() {
    m_SecurityService.invalidateAuthentication(m_ServiceAgent);

    m_ServiceAgent = null;
    m_SecurityService = null;
    m_UserAgentCache = null;
    m_RoleCache = null;
    m_RolePermissionsCache = null;
    m_Active = false;
    m_BundleMessages = null;
    m_Services = null;
    return true;
  }//deactivate

  public Agent getAgent(Agent caller, String id)
      throws SecurityException, NoSuchAgentException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_AGENT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgent()", ex);
    }
    ResultSet rs = null;
    Agent a = null;
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", id);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgent", params);
      if (rs.next()) {
        if (rs.getInt(2) == 0) {
          a = m_ServiceAgentCache.getAgent(rs.getString(1));
        } else {
          a = m_UserAgentCache.getAgent(rs.getString(1));
        }
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getAgent()", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    if (a == null) {
      throw new NoSuchAgentException(id);
    }
    return a;
  }//getAgent

  public Set<Agent> getAgents(Agent caller)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_AGENTS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgents()", ex);
    }
    Set<Agent> s = new HashSet<Agent>();
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgents", null);
      while (rs.next()) {
        Agent a;
        if (rs.getInt(2) == 0) {
          a = m_ServiceAgentCache.getAgent(rs.getString(1));
        } else {
          a = m_UserAgentCache.getAgent(rs.getString(1));
        }
        s.add(a);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getAgents()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(s);
  }//getAgents

  public Set<Agent> getAgentsWithRole(Agent caller, Role r)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_AGENTS_WITH_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgentsWithRole()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("rolename", r.getIdentifier());
    Set<Agent> s = new HashSet<Agent>();
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgentsWithRole", params);
      while (rs.next()) {
        Agent a;
        if (rs.getInt(3) == 0) {
          a = m_ServiceAgentCache.getAgent(rs.getString(1));
        } else {
          a = m_UserAgentCache.getAgent(rs.getString(1));
        }
        s.add(a);

      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getAgents", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(s);
  }//getAgentsWithRole

  public Agent registerAgent(Agent caller, AgentIdentifier aid, boolean user) throws SecurityException {
    if (!aid.isLocal()) {
      throw new IllegalArgumentException("Non-local agent identifier.");
    }
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REGISTER_AGENT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "registerAgent()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getName());
    params.put("agent_type", (user) ? "1" : "0");
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("registerAgent", params);
      if (user) {
        return m_UserAgentCache.getAgent(aid.getName());
      } else {
        return m_ServiceAgentCache.getAgent(aid.getName());
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "registerAgent()", ex);
      return null;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//registerAgent

  public boolean unregisterAgent(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException {
    final String id = aid.getIdentifier();
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), UNREGISTER_AGENT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "unregisterAgent()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getName());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("unregisterAgent", params);
      //invalidate in both, just to be sure we catch the right one.
      m_UserAgentCache.invalidate(id);
      m_ServiceAgentCache.invalidate(id);

      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "unregisterAgent()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//unregisterAgent

  public boolean createAuthentication(Agent caller, Agent a, String credential)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), CREATE_AUTHENTICATION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "createAuthentication()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    String name = a.getAgentIdentifier().getName();
    LibraryConnection lc = null;
    try {
      StoreEntry se = StoreEntry.createStoreEntry(name, credential);
      params.put("agent_id", name);
      params.put("credential", se.toString());
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("createAgentCredential", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createAuthentication()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//createAuthentication

  public boolean updateAuthentication(Agent caller, Agent a, String credential)
      throws SecurityException {
    if (!(caller.equals(a) && m_SecurityService.isAuthentic(caller))) {
      try {
        m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), UPDATE_AUTHENTICATION);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, "updateAuthentication()", ex);
      }
    }
    Map<String, String> params = new HashMap<String, String>();
    String name = a.getAgentIdentifier().getName();
    LibraryConnection lc = null;
    try {
      StoreEntry se = StoreEntry.createStoreEntry(name, credential);
      params.put("agent_id", name);
      params.put("credential", se.toString());
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("updateAgentCredential", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "updateAuthentication()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    //invalidate cache
    m_SecurityService.invalidateCachedCredential(a);
    return true;
  }//updateAuthentication

  public void removeAuthentication(Agent caller, Agent a)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REMOVE_AUTHENTICATION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "removeAuthentication()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", a.getAgentIdentifier().getName());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("removeAuthentication", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "removeAuthentication()", ex);
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//removeAuthentication

  public Permission getPermission(Permission p) {
    return m_PermissionCache.getPermission(p.getIdentifier());
  }//getPermission

  public Permission getPermit(Permission p) {
    return m_PermitCache.getPermission(p.getIdentifier());
  }//getPermit

  public Permission getDeny(Permission p) {
    return m_DenyCache.getPermission(p.getIdentifier());
  }//getDeny

  public Permission getPermission(Agent caller, String id, boolean permit)
      throws SecurityException, NoSuchPermissionException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getPermission()", ex);
    }
    if (permit) {
      return m_PermitCache.getPermission(id);
    } else {
      return m_DenyCache.getPermission(id);
    }
  }//getPermission

  public Set<Permission> getPermissions(Agent caller)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_PERMISSIONS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getPermissions()", ex);
    }
    Set<Permission> s = new HashSet<Permission>();
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("listPermissions", null);
      while (rs.next()) {
        s.add(m_PermissionCache.getPermission(rs.getInt(1), rs.getString(2)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getPermissions()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(s);
  }//getPermissions


  public Permission createPermission(Agent caller, String id)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), CREATE_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "createPermission()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("name", id);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("createPermission", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createPermission()", ex);
      return null;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    //load it from the magic cache, that will retrieve the created id for us
    return m_PermissionCache.getPermission(id);
  }//createPermission

  public boolean destroyPermission(Agent caller, Permission p)
      throws SecurityException, NoSuchPermissionException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), DESTROY_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "destroyPermission()", ex);
    }

    Map<String, String> params = new HashMap<String, String>();
    params.put("permission_id", "" + ((PermissionImpl) p).getNumber());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("destroyPermission", params);
      m_PermissionCache.invalidate(p);
      m_SecurityService.updateAuthorizations(m_ServiceAgent);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "destroyPermission()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//destroyPermission

  public Role getRole(Agent caller, String id)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getRole()", ex);
    }
    return m_RoleCache.getRole(id);
  }//getRole

  public Role createRole(Agent caller, String id)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), CREATE_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "createRole()", ex);
    }
    Map<String, String> params = new HashMap<String, String>();
    params.put("name", id);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("createRole", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "createRole()", ex);
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    //load it from the magic cache, that will retrieve the created id for us
    return m_RoleCache.getRole(id);
  }//createRole

  public void renameRole(Agent caller, Role r, String newid)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), RENAME_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "renameRole()", ex);
    }
    RoleImpl ri = m_RoleCache.getRole(r.getIdentifier());

    Map<String, String> params = new HashMap<String, String>();
    params.put("name", newid);
    params.put("role_id", "" + ri.getNumber());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("renameRole", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "renameRole()", ex);
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    m_RoleCache.invalidate(ri);
  }//renameRole

  public boolean destroyRole(Agent caller, Role r)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), DESTROY_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "destroyRole()", ex);
    }
    RoleImpl ri = m_RoleCache.getRole(r.getIdentifier());

    Map<String, String> params = new HashMap<String, String>();
    params.put("role_id", "" + ri.getNumber());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("destroyRole", params);
      m_SecurityService.updateAuthorizations(m_ServiceAgent, ri);
      m_RoleCache.invalidate(ri);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "destroyRole()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//destroyRole

  public boolean grantRole(Agent caller, Agent a, Role r)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GRANT_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "grantRole()", ex);
    }
    RoleImpl ri = m_RoleCache.getRole(r.getIdentifier());
    Map<String, String> params = new HashMap<String, String>();
    params.put("role_id", "" + ri.getNumber());
    params.put("agent_id", a.getAgentIdentifier().getName());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("grantRole", params);
      m_SecurityService.updateAuthorizations(m_ServiceAgent, a);
    } catch (Exception ex) {
      Activator.log().error("grantRole()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//grantRole

  public boolean revokeRole(Agent caller, Agent a, Role r)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REVOKE_ROLE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "revokeRole()", ex);
    }
    RoleImpl ri = m_RoleCache.getRole(r.getIdentifier());
    Map<String, String> params = new HashMap<String, String>();
    params.put("role_id", "" + ri.getNumber());
    params.put("agent_id", a.getAgentIdentifier().getName());
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("revokeRole", params);
      m_SecurityService.updateAuthorizations(m_ServiceAgent, a);
    } catch (Exception ex) {
      Activator.log().error("revokeRole()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//revokeRole

  public Set<Role> getAgentRoles(Agent caller, Agent a) throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_AGENT_ROLES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgentRoles()", ex);
    }
    Set<Role> s = new HashSet<Role>();
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", a.getAgentIdentifier().getName());
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgentRoles", params);
      while (rs.next()) {
        s.add(m_RoleCache.getRole(rs.getInt(1), rs.getString(2)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getAgentRoles()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(s);
  }//getAgentRoles

  public Set<Role> getRoles(Agent caller)
      throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_ROLES);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getAgentRoles()", ex);
    }
    Set<Role> s = new HashSet<Role>();
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("listRoles", null);
      while (rs.next()) {
        s.add(m_RoleCache.getRole(rs.getInt(1), rs.getString(2)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "getAgentRoles()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(s);
  }//getAgentRoles

  public Set<Permission> getRolePermissions(Agent caller, Role r)
      throws NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), GET_ROLE_PERMISSIONS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "getRolePermissions()", ex);
    }
    return m_RolePermissionsCache.getPermissions((RoleImpl) r).getAsUnmodifiableSet();
  }//getRolePermissions

  public boolean addRolePermission(Agent caller, Role r, Permission p)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), ADD_ROLE_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "addPermission()", ex);
    }
    return manipulateRolePermission("addPermission()", r, p, true, false);
  }//addPermission

  public boolean addRolePermissions(Agent caller, Role r, Set s)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), ADD_ROLE_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "addPermissions()", ex);
    }
    for (Iterator iterator = s.iterator(); iterator.hasNext();) {
      Object o = iterator.next();
      if (o instanceof Permission) {
        if (!manipulateRolePermission("addPermissions()", r, (Permission) o, true, iterator.hasNext())) {
          return false;
        }
      }
    }
    return true;
  }//addPermissions

  public boolean removeRolePermission(Agent caller, Role r, Permission p)
      throws SecurityException, NoSuchRoleException {

    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REMOVE_ROLE_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "removePermission():", ex);
    }
    return manipulateRolePermission("removePermission()", r, p, false, false);
  }//removePermission

  public boolean removeRolePermissions(Agent caller, Role r, Set s)
      throws SecurityException, NoSuchRoleException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REMOVE_ROLE_PERMISSION);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, "removePermission():", ex);
    }
    for (Iterator iterator = s.iterator(); iterator.hasNext();) {
      Object o = iterator.next();
      if (o instanceof Permission) {
        if (!manipulateRolePermission("removePermissions()", r, (PermissionImpl) o, false, iterator.hasNext())) {
          return false;
        }
      }
    }
    return true;
  }//addPermissions

  private boolean manipulateRolePermission(String logmsg, Role r, Permission p, boolean add, boolean delayupdate) {
    //TODO: Probably add a check first if the manipulation makes sense
    final RoleImpl ri = (RoleImpl) r;
    Map<String, String> params = new HashMap<String, String>();
    params.put("role_id", "" + ri.getNumber());
    params.put("permission_id", "" + ((PermissionImpl) p).getNumber());
    if (p instanceof Permit) {
      params.put("permit", "1");
    } else {
      params.put("permit", "0");
    }
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      if (add) {
        lc.executeUpdate("addRolePermission", params);
      } else {
        lc.executeUpdate("removeRolePermission", params);
      }
      if (!delayupdate) {
        m_SecurityService.updateAuthorizations(m_ServiceAgent, ri);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, logmsg, ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
    return true;
  }//manipulatePermission

  private static Action GET_AGENT = new Action("getAgent");
  private static Action REGISTER_AGENT = new Action("registerAgent");
  private static Action UNREGISTER_AGENT = new Action("unregisterAgent");
  private static Action GET_AGENTS = new Action("getAgents");
  private static Action GET_AGENTS_WITH_ROLE = new Action("getAgentsWithRole");
  private static Action CREATE_AUTHENTICATION = new Action("createAuthentication");
  private static Action UPDATE_AUTHENTICATION = new Action("updateAuthentication");
  private static Action REMOVE_AUTHENTICATION = new Action("removeAuthentication");
  private static Action GET_ROLE = new Action("getRole");
  private static Action CREATE_ROLE = new Action("createRole");
  private static Action RENAME_ROLE = new Action("renameRole");
  private static Action DESTROY_ROLE = new Action("destroyRole");
  private static Action GRANT_ROLE = new Action("grantRole");
  private static Action REVOKE_ROLE = new Action("revokeRole");
  private static Action GET_ROLES = new Action("getRoles");
  private static Action GET_AGENT_ROLES = new Action("getAgentRoles");
  private static Action GET_ROLE_PERMISSIONS = new Action("getRolePermissions");
  private static Action ADD_ROLE_PERMISSION = new Action("addRolePermission");
  private static Action REMOVE_ROLE_PERMISSION = new Action("removeRolePermission");
  private static Action GET_PERMISSION = new Action("getPermission");
  private static Action GET_PERMISSIONS = new Action("getPermissions");
  private static Action CREATE_PERMISSION = new Action("createPermission");
  private static Action DESTROY_PERMISSION = new Action("removePermission");

  private static Action[] ACTIONS = {
      GET_AGENT, REGISTER_AGENT, UNREGISTER_AGENT, GET_AGENTS, GET_AGENTS_WITH_ROLE,
      CREATE_AUTHENTICATION, UPDATE_AUTHENTICATION, REMOVE_AUTHENTICATION, GET_ROLE,
      CREATE_ROLE, DESTROY_ROLE, GRANT_ROLE, REVOKE_ROLE, GET_ROLES, GET_AGENT_ROLES,
      GET_ROLE_PERMISSIONS, ADD_ROLE_PERMISSION, REMOVE_ROLE_PERMISSION, GET_PERMISSION,
      GET_PERMISSIONS, CREATE_PERMISSION, DESTROY_PERMISSION, RENAME_ROLE
  };

}//class SecurityManagementServiceImpl
