/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.security.model.AuthorizationRule;
import net.coalevo.security.model.Policy;
import net.coalevo.security.service.PolicyXMLService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Provides for storage and retrieval of XML based
 * {@link net.coalevo.security.model.Policy} persistency.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PolicyXMLServiceImpl
    extends BaseService
    implements PolicyXMLService {

  private Marker m_LogMarker = MarkerFactory.getMarker(PolicyXMLServiceImpl.class.getName());

  private static GenericObjectPool.Config c_PoolConfig;
  private GenericObjectPool m_Handlers;

  static {
    c_PoolConfig = new GenericObjectPool.Config();
    c_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    c_PoolConfig.maxWait = -1;
    c_PoolConfig.maxActive = 3;
    c_PoolConfig.timeBetweenEvictionRunsMillis = -1; //no eviction thread
  }//initializer

  public PolicyXMLServiceImpl() {
    super(PolicyXMLService.class.getName(), ACTIONS);
  }//constructor

  public boolean activate(BundleContext bc) {
    m_Handlers = new GenericObjectPool(new PolicyHandlerFactory(), c_PoolConfig);
    return true;
  }//activate

  public boolean deactivate() {
    m_Handlers = null;
    return true;
  }//deactivate

  public void toXML(Writer w, Policy p)
      throws IOException {
    PolicyImpl pi = (PolicyImpl) p;
    w.write(Tokens.XMLDOC_HEADER);
    w.write(Tokens.POLICY_START_PRE);
    w.write(p.getIdentifier());
    w.write(Tokens.POLICY_START_POST);

    for (Iterator iter = pi.getActions().iterator(); iter.hasNext();) {
      Action a = (Action) iter.next();
      AuthorizationRule r = pi.getRuleFor(a);
      w.write(Tokens.ENTRY_PRE);
      w.write(a.getIdentifier());
      w.write(Tokens.ENTRY_POST);
      w.write(Tokens.RULE_START);
      w.write(r.toSARL().replaceAll("&", "&amp;"));
      w.write(Tokens.RULE_END);
      w.write(Tokens.ENTRY_END);
    }
    w.write(Tokens.POLICY_END);
    w.flush();
  }//marshallPolicy

  public String toXML(Policy p)
      throws IOException {
    final StringWriter w = new StringWriter();
    toXML(w, p);
    w.flush();
    return w.toString();
  }//toXML

  public Policy fromXML(Reader ir)
      throws IOException {
    try {
      PolicyHandler ph = (PolicyHandler) m_Handlers.borrowObject();
      try {
        SAXParserFactory factory = Activator.getServices().getSAXParserFactory(ServiceMediator.WAIT_UNLIMITED);
        factory.setValidating(false);
        XMLReader parser = factory.newSAXParser().getXMLReader();
        InputSource src = new InputSource(ir);

        parser.setContentHandler(ph);
        parser.parse(src);
        return new PolicyImpl(null, ph.getPolicyIdentifier(), ph.getPolicyEntries());
      } finally {
        m_Handlers.returnObject(ph);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"fromXML()", ex);
      final IOException ioex = new IOException();
      ioex.initCause(ex);
      throw ioex;
    }
  }//unmarshallPolicy

  public Policy fromXML(String xml)
      throws IOException {
    final StringReader r = new StringReader(xml);
    return fromXML(r);
  }//fromXML

  private class PolicyHandlerFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new PolicyHandler();
    }//makeObject

    public void passivateObject(Object o) {
      if (o instanceof PolicyHandler) {
        ((PolicyHandler) o).reset();
      }
    }//passivateObject

  }//inner class PolicyHandlerFactory

  private class PolicyHandler
      extends DefaultHandler {

    //state
    protected int m_State = STATE_UNDEFINED;
    protected StringBuilder m_Buffer;
    protected String m_PolicyIdentifier;
    protected Action m_TempAction;
    protected AuthorizationRule m_TempRule;
    protected Map<Action, AuthorizationRule> m_Entries;

    public String getPolicyIdentifier() {
      return m_PolicyIdentifier;
    }//getPolicyIdentifier

    public Map<Action, AuthorizationRule> getPolicyEntries() {
      return m_Entries;
    }//getEntries

    public void reset() {
      m_Buffer = null;
      m_PolicyIdentifier = null;
      m_TempAction = null;
      m_TempRule = null;
      m_State = STATE_UNDEFINED;
    }//reset

    public void startElement(String uri, String name,
                             String qualifiedName, Attributes attributes)
        throws SAXException {
      //Activator.log().debug("startElement()::" + m_State + "::" + name);
      try {
        switch (m_State) {
          case STATE_UNDEFINED:
            if (Tokens.POLICY.equals(name)) {
              m_Entries = new HashMap<Action, AuthorizationRule>();
              handlePolicyAttributes(attributes);
              m_State = STATE_POLICY;
            } else {
              throw new SAXException("Root element not found.");
            }
            break;
          case STATE_POLICY:
            if (Tokens.ENTRY.equals(name)) {
              handleEntryAttributes(attributes);
              m_State = STATE_ENTRY;
            } else {
              throw new SAXException("Unknown element " + name);
            }
            break;
          case STATE_ENTRY:
            if (Tokens.AUTH_RULE.equals(name)) {
              m_Buffer = new StringBuilder();
              m_State = STATE_AUTHRULE;
            } else {
              throw new SAXException("Unknown element " + name);
            }
            break;
        }
      } catch (Exception e) {
        throw new SAXException(e);
      }
    }//startElement

    public void endElement(String namespaceURI, String localName, String qName)
        throws SAXException {
      //Activator.log().debug("endElement()::" + m_State + "::" + localName);
      switch (m_State) {
        case STATE_POLICY:
          m_State = STATE_UNDEFINED;
          break;
        case STATE_ENTRY:
          m_Entries.put(m_TempAction, m_TempRule);
          m_TempAction = null;
          m_TempRule = null;
          m_State = STATE_POLICY;
          break;
        case STATE_AUTHRULE:
          try {
            m_TempRule = AuthorizationRuleImpl.create(m_Buffer.toString());
          } catch (Exception ex) {
            throw new SAXException("Rule not SARL conform.", ex);
          }
          m_Buffer = null;
          m_State = STATE_ENTRY;
          break;
      }
    }//endElement

    public void characters(char[] chars, int start, int length)
        throws SAXException {
      switch (m_State) {
        case STATE_AUTHRULE:
          m_Buffer.append(chars, start, length);
          break;
      }
    }//characters

    private final void handlePolicyAttributes(Attributes attr)
        throws SAXException {
      //Activator.log().debug("handlePolicyAttributes()::" + attr.toString());
      //1. package
      String str = attr.getValue("", Tokens.IDENTIFIER);
      if (str == null || str.length() == 0) {
        throw new SAXException("Identifier missing.");
      } else {
        m_PolicyIdentifier = str;
      }
    }//handlePolicyAttributes

    private final void handleEntryAttributes(Attributes attr)
        throws SAXException {
      //Activator.log().debug("handleEntryAttributes()::" + attr.toString());
      //1. package
      String str = attr.getValue("", Tokens.ACTION_ID);
      if (str == null || str.length() == 0) {
        throw new SAXException("Action identifier missing.");
      } else {
        m_TempAction = new Action(str);
      }
    }//handleEntryAttributes


    private static final int STATE_UNDEFINED = -1;
    private static final int STATE_POLICY = 1;
    private static final int STATE_ENTRY = 2;
    private static final int STATE_AUTHRULE = 3;

  }//PolicyHandler

  /**
   * Inner class defining the tokens to be encountered in
   * a policy document.
   */
  private final class Tokens {

    //tags
    static final String POLICY = "policy";
    static final String IDENTIFIER = "identifier";
    static final String ENTRY = "entry";
    //attributes
    static final String ACTION_ID = "action-id";
    static final String AUTH_RULE = "auth-rule";

    //prefix
    static final String XMLDOC_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    static final String POLICY_START_PRE = "<policy identifier=\"";
    static final String POLICY_START_POST = "\">";
    static final String POLICY_END = "</policy>";
    static final String ENTRY_PRE = "<entry action-id=\"";
    static final String ENTRY_POST = "\">";
    static final String ENTRY_END = "</entry>";
    static final String RULE_START = "<auth-rule>";
    static final String RULE_END = "</auth-rule>";

  }//inner class Tokens

  public static final Action[] ACTIONS = new Action[]{};

}//class PolicyXMLServiceImpl
