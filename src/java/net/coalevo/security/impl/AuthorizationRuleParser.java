// $ANTLR 2.7.5 (20050128): "sarl.g" -> "AuthorizationRuleParser.java"$

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import antlr.*;
import antlr.collections.AST;
import antlr.collections.impl.BitSet;

/**
 * Provides a <tt>Parser</tt> for SARL that
 * was automatically generated from a grammar
 * by ANTLR.
 *
 * @author Dieter Wimberger (grammar)
 * @author ANTLR
 */
class AuthorizationRuleParser extends antlr.LLkParser implements AuthorizationRuleLexerTokenTypes {

  protected AuthorizationRuleParser(TokenBuffer tokenBuf, int k) {
    super(tokenBuf, k);
    tokenNames = _tokenNames;
    buildTokenTypeASTClassMap();
    astFactory = new ASTFactory(getTokenTypeToASTClassMap());
  }

  public AuthorizationRuleParser(TokenBuffer tokenBuf) {
    this(tokenBuf, 1);
  }

  protected AuthorizationRuleParser(TokenStream lexer, int k) {
    super(lexer, k);
    tokenNames = _tokenNames;
    buildTokenTypeASTClassMap();
    astFactory = new ASTFactory(getTokenTypeToASTClassMap());
  }

  public AuthorizationRuleParser(TokenStream lexer) {
    this(lexer, 1);
  }

  public AuthorizationRuleParser(ParserSharedInputState state) {
    super(state, 1);
    tokenNames = _tokenNames;
    buildTokenTypeASTClassMap();
    astFactory = new ASTFactory(getTokenTypeToASTClassMap());
  }

  public final void expr() throws RecognitionException, TokenStreamException {

    returnAST = null;
    ASTPair currentAST = new ASTPair();
    AST expr_AST = null;

    //try {      // for error handling
    pred();
    astFactory.addASTChild(currentAST, returnAST);
    AST tmp1_AST = null;
    tmp1_AST = astFactory.create(LT(1));
    astFactory.addASTChild(currentAST, tmp1_AST);
    match(SEMI);
    expr_AST = (AST) currentAST.root;
    //}
    //catch (RecognitionException ex) {
    //  reportError(ex);
    //  recover(ex, _tokenSet_0);
    //}
    returnAST = expr_AST;
  }

  public final void pred() throws RecognitionException, TokenStreamException {

    returnAST = null;
    ASTPair currentAST = new ASTPair();
    AST pred_AST = null;

    //try {      // for error handling
    switch (LA(1)) {
      case LPAREN: {
        match(LPAREN);
        pred();
        astFactory.addASTChild(currentAST, returnAST);
        {
          switch (LA(1)) {
            case AND: {
              AST tmp3_AST = null;
              tmp3_AST = astFactory.create(LT(1));
              astFactory.makeASTRoot(currentAST, tmp3_AST);
              match(AND);
              break;
            }
            case OR: {
              AST tmp4_AST = null;
              tmp4_AST = astFactory.create(LT(1));
              astFactory.makeASTRoot(currentAST, tmp4_AST);
              match(OR);
              break;
            }
            default: {
              throw new NoViableAltException(LT(1), getFilename());
            }
          }
        }
        pred();
        astFactory.addASTChild(currentAST, returnAST);
        match(RPAREN);
        pred_AST = (AST) currentAST.root;
        break;
      }
      case PERMISSION: {
        AST tmp6_AST = null;
        tmp6_AST = astFactory.create(LT(1));
        astFactory.addASTChild(currentAST, tmp6_AST);
        match(PERMISSION);
        pred_AST = (AST) currentAST.root;
        break;
      }
      case ROLE: {
        AST tmp7_AST = null;
        tmp7_AST = astFactory.create(LT(1));
        astFactory.addASTChild(currentAST, tmp7_AST);
        match(ROLE);
        pred_AST = (AST) currentAST.root;
        break;
      }
      default: {
        throw new NoViableAltException(LT(1), getFilename());
      }
    }
    //}
    //catch (RecognitionException ex) {
    //  reportError(ex);
    //  recover(ex, _tokenSet_1);
    //}
    returnAST = pred_AST;
  }


  public static final String[] _tokenNames = {
      "<0>",
      "EOF",
      "<2>",
      "NULL_TREE_LOOKAHEAD",
      "PERMISSION",
      "ROLE",
      "AND",
      "OR",
      "LPAREN",
      "RPAREN",
      "SEMI",
      "WS"
  };

  protected void buildTokenTypeASTClassMap() {
    tokenTypeToASTClassMap = null;
  }

  ;

  private static final long[] mk_tokenSet_0() {
    long[] data = {2L, 0L};
    return data;
  }

  public static final BitSet _tokenSet_0 = new BitSet(mk_tokenSet_0());

  private static final long[] mk_tokenSet_1() {
    long[] data = {1728L, 0L};
    return data;
  }

  public static final BitSet _tokenSet_1 = new BitSet(mk_tokenSet_1());

}
