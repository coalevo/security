/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.File;

class SecurityInit {

  private List<String> m_Roles;
  private List<String> m_Services;

  private SecurityInit(){};

  public List<String> getRoles() {
    return m_Roles;
  }//getRoles

  public List<String> getServices() {
    return m_Services;
  }//getServices

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("SecurityInit:{");
    sb.append("Roles=").append(m_Roles.toString());
    sb.append(", Services=").append(m_Services.toString());
    sb.append("}");
    return sb.toString();
  }//toString

  public static SecurityInit create(File f) throws Exception {
    SecurityInit si = new SecurityInit();
    Properties p = new Properties();
    p.load(new FileInputStream(f));

    String[] roles = p.getProperty("roles").split(",");
    System.out.println(java.util.Arrays.toString(roles));
    String[] services = p.getProperty("services").split(",");

    List<String> tmps = new ArrayList<String>();
    for(String s:roles) {
      if(!tmps.contains(s)) {
        tmps.add(s);
      }
    }
    si.m_Roles = Collections.unmodifiableList(tmps);
    tmps = new ArrayList<String>();

    for(String s:services) {
      if(!tmps.contains(s)) {
        tmps.add(s);
      }
    }
    si.m_Services = Collections.unmodifiableList(tmps);
    return si;
  }//init

}//SecurityInit
