/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Identifiable;
import net.coalevo.security.model.Permission;

/**
 * Provides a package local implementation of
 * {@link Permission}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PermissionImpl
    implements Identifiable, Permission {

  protected final String m_Identifier;
  protected final int m_Number;

  public PermissionImpl(int num, String id) {
    m_Number = num;
    m_Identifier = id;
  }//constructor

  public int getNumber() {
    return m_Number;
  }//getNumber

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public String toString() {
    return new StringBuilder("Permission [")
        .append(m_Number)
        .append(",")
        .append(m_Identifier)
        .append("]")
        .toString();
  }//toString

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || !(o instanceof PermissionImpl)) return false;

    final PermissionImpl that = (PermissionImpl) o;

    if (m_Number != that.m_Number) return false;
    if (m_Identifier != null ? !m_Identifier.equals(that.m_Identifier) : that.m_Identifier != null) return false;

    return true;
  }//equals

  public int hashCode() {
    int result;
    result = (m_Identifier != null ? m_Identifier.hashCode() : 0);
    result = 29 * result + m_Number;
    return result;
  }//hashCode


}//class PermissionImpl
