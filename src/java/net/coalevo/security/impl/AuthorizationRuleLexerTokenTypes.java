// $ANTLR 2.7.5 (20050128): "sarl.g" -> "AuthorizationRuleImpl.java"$

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

interface AuthorizationRuleLexerTokenTypes {

  int EOF = 1;
  int NULL_TREE_LOOKAHEAD = 3;
  int PERMISSION = 4;
  int ROLE = 5;
  int AND = 6;
  int OR = 7;
  int LPAREN = 8;
  int RPAREN = 9;
  int SEMI = 10;
  int WS = 11;
}
