/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.foundation.model.Messages;
import net.coalevo.security.model.*;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityService;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Implements a {@link SecurityStore} based
 * {@link net.coalevo.security.service.PolicyService}.
 * <p/>
 * <p/>
 * This policy service will store inside the database associated with the
 * {@link SecurityStore}.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see SecurityStore
 */
class PolicyServiceImpl
    extends BaseService
    implements PolicyService {

  private Marker m_LogMarker = MarkerFactory.getMarker(PolicyServiceImpl.class.getName());
  private Agent m_ServiceAgent;
  protected PolicyImpl m_Policy;
  protected Map<String,PolicyImpl> m_Policies;
  private AuthorizationRuleImpl m_BaseRule;
  private boolean m_Active;
  private SecurityService m_SecurityService;
  private SecurityStore m_SecurityStore;
  private Messages m_BundleMessages;

  public PolicyServiceImpl(SecurityStore store) {
    super(PolicyService.class.getName(), ACTIONS);
    m_SecurityStore = store;
  }//constructor

  public boolean activate(BundleContext bc) {
    if (m_Active) {
      Activator.log().error(m_LogMarker,"activate()::Already active.");
      return false;
    }
    m_BundleMessages = Activator.getBundleMessages();
    ServiceMediator services = Activator.getServices();
    m_Policies = new HashMap<String,PolicyImpl>();

    //2. Authenticate ourself
    try {
      m_SecurityService = services.getSecurityService();
      if (m_SecurityService == null) {
        Activator.log().error(m_LogMarker,m_BundleMessages.get("PolicyServiceImpl.exceptions.service", "service", "SecurityService"));
        return false;
      }
      m_ServiceAgent = m_SecurityService.authenticate(this);
    } catch (AuthenticationException ex) {
      Activator.log().error(m_LogMarker,"activate()::Could not authenticate this PolicyService.", ex);
      return false;
    }

    //3. prepare base rule.
    String baserule = m_SecurityStore.getBaseAdminAuthorizationRule();
    try {
      m_BaseRule = (AuthorizationRuleImpl) AuthorizationRuleImpl.create(baserule);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"activate()::Could not create basic authorization rule.", ex);
      return false;
    }

    //4.Initialize store if it is new
    String identifier = PolicyService.class.getName();
    if (m_SecurityStore.isNew()) {
      //create index entry
      if (!doCreatePolicy(identifier)) {
        return false;
      }
      //create policy entries
      if (!createPolicyEntry(identifier, LEASE_POLICY.getIdentifier(), m_BaseRule.getRule()) ||
          !createPolicyEntry(identifier, DELETE_POLICY.getIdentifier(), m_BaseRule.getRule()) ||
          !createPolicyEntry(identifier, CREATE_POLICY.getIdentifier(), m_BaseRule.getRule()) ||
          !createPolicyEntry(identifier, PUT_ENTRY.getIdentifier(), m_BaseRule.getRule()) ||
          !createPolicyEntry(identifier, REMOVE_ENTRY.getIdentifier(), m_BaseRule.getRule())
          ) {
        return false;
      }
    }
    //6. obtain own policy ref
    m_Policy = (PolicyImpl) loadPolicy(identifier);
    m_Policy.incrementRefCount();
    m_Policies.put(identifier, m_Policy);

    m_Active = true;
    return true;
  }//activate


  public boolean deactivate() {
    if (m_Policy != null) {
      releasePolicy(m_Policy);
    }
    m_Policies.clear();
    m_Policy = null;
    m_Policies = null;
    m_BaseRule = null;
    m_SecurityService.invalidateAuthentication(m_ServiceAgent);
    m_ServiceAgent = null;
    m_SecurityService = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public AuthorizationRule getBaseAuthorizationRule() {
    return m_BaseRule;
  }//getBaseAuthorizationRule

  public AuthorizationRule createAuthorizationRule(String stmt)
      throws SARLException {
    try {
      return AuthorizationRuleImpl.create(stmt);
    } catch (Exception e) {
      Activator.log().error(m_LogMarker,"createAuthorizationRule()", e);
      throw new SARLException(e);
    }
  }//createAuthorizationRule

  public Set<String> listAvailablePolicies() {
    Set<String> st = new HashSet<String>();
    //go through the index
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("listPolicies", null);
      while (rs.next()) {
        st.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listPolicies()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return Collections.unmodifiableSet(st);
  }//listAvailablePolicies

  public boolean isPolicyAvailable(String identifier) {
    for (Iterator iter = listAvailablePolicies().iterator(); iter.hasNext();) {
      if (identifier.equals(iter.next())) {
        return true;
      }
    }
    return false;
  }//isPolicyAvailable

  public Policy leasePolicy(Agent caller, String identifier)
      throws SecurityException, NoSuchPolicyException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), LEASE_POLICY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker,"leasePolicy():", ex);
    }
    PolicyImpl p = m_Policies.get(identifier);
    if (p == null) {
      //load it
      p = loadPolicy(identifier);
      if (p == null) {
        throw new NoSuchPolicyException(identifier);
      } else {
        m_Policies.put(identifier, p);
      }
    }
    p.incrementRefCount();
    return p;
  }//leasePolicy

  public void releasePolicy(Policy p) {
    if (isAssociatedPolicy(p)) {
      PolicyImpl pi = (PolicyImpl) p;
      pi.decrementRefCount();
      if (!pi.isReferenced()) {
        m_Policies.remove(p.getIdentifier());
      }
    }
  }//releasePolicy


  public Policy createPolicy(Agent caller, String identifier) throws SecurityException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), CREATE_POLICY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker,"createPolicy():", ex);
      return null;
    }
    synchronized (m_Policies) {
      if (m_Policies.containsKey(identifier)) {
        return (Policy) m_Policies.get(identifier);
      } else {
        //1. create policy
        if (doCreatePolicy(identifier)) {
          PolicyImpl p = new PolicyImpl(this, identifier);
          m_Policies.put(identifier, p);
          return p;
        }
      }
    }
    return null;
  }//createPolicy

  public boolean deletePolicy(Agent caller, String identifier)
      throws SecurityException, NoSuchPolicyException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), DELETE_POLICY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker,"removePolicy():", ex);
      return false;
    }
    synchronized (m_Policies) {
      if (m_Policies.containsKey(identifier)) {
        //1. policy in use?
        PolicyImpl pi = (PolicyImpl) m_Policies.get(identifier);
        if (pi.isReferenced()) {
          throw new IllegalStateException("Policy is still referenced.");
        } else {
          //1. remove reference
          if (doDeletePolicy(identifier)) {
            m_Policies.remove(identifier);
            return true;
          } else {
            return false;
          }
        }
      } else {
        return doDeletePolicy(identifier);
      }//end else block
    }//end synchronized block
  }//deletePolicy

  public boolean putPolicyEntry(Agent caller, Policy p, Action a, AuthorizationRule r)
      throws SecurityException, NoSuchPolicyException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), PUT_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker,"putPolicyEntry():", ex);
      return false;
    }
    //1. Ensure the policy was leased from this service
    if (isAssociatedPolicy(p)) {
      PolicyImpl policy = (PolicyImpl) p;
      //check whether update or create
      if (policy.hasEntry(a)) {
        if (!updatePolicyEntry(p.getIdentifier(), a.getIdentifier(), r.toString())) {
          return false;
        }
      } else {
        if (!createPolicyEntry(p.getIdentifier(), a.getIdentifier(), r.toString())) {
          return false;
        }
      }
      //replace reference
      if (policy.putEntry(a, r)) {
        Activator.log().info(m_LogMarker,
            m_BundleMessages.get("PolicyServiceImpl.policy.entry.update", "aid", a.getIdentifier(), "rule", r.toString())
        );
      } else {
        Activator.log().info(m_LogMarker,
            m_BundleMessages.get("PolicyServiceImpl.policy.entry.add", "aid", a.getIdentifier(), "rule", r.toString())
        );
      }
      return true;
    } else {
      throw new NoSuchPolicyException(p);
    }
  }//putPolicyEntry

  public boolean removePolicyEntry(Agent caller, Policy p, Action a)
      throws SecurityException, NoSuchPolicyException, NoSuchActionException {
    try {
      m_Policy.ensureAuthorization(m_SecurityService.getAuthorizations(caller), REMOVE_ENTRY);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker,"removePolicyEntry():", ex);
      return false;
    }
    //1. Ensure the policy was leased from this service
    if (isAssociatedPolicy(p)) {
      PolicyImpl policy = (PolicyImpl) p;
      if (!removePolicyEntry(p.getIdentifier(), a.getIdentifier())) {
        return false;
      }
      if (policy.removeEntry(a)) {
        Activator.log().info(m_LogMarker,
            m_BundleMessages.get("PolicyServiceImpl.policy.entry.remove", "aid", a.getIdentifier())
        );
        return true;
      } else {
        throw new NoSuchActionException(a);
      }
    } else {
      throw new NoSuchPolicyException(p);
    }
  }//removePolicyEntry


  /**
   * Tests if the given {@link net.coalevo.security.model.Policy} is associated with
   * this <tt>PolicyService</tt>.
   *
   * @param p a {@link net.coalevo.security.model.Policy}.
   * @return true if associated, false otherwise.
   */
  private final boolean isAssociatedPolicy(Policy p) {
    return m_Policies.containsValue((PolicyImpl)p);
  }//isAssociatedPolicy

  // Store manipulating functionality
  private boolean doCreatePolicy(String id) {
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, id);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("createPolicy", params);
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"doCreatePolicy()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//doCreatePolicy

  private boolean doDeletePolicy(String id) {
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, id);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("deletePolicy", params);
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"doDeletePolicy()", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//doDeletePolicy


  private boolean createPolicyEntry(String pid, String aid, String authrule) {
    //2. create policy index entry
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, pid);
    params.put(ACTION_ID, aid);
    params.put(AUTHRULE, authrule);
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("createPolicyEntry", params);
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createPolicyEntry()::", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//createPolicyEntry

  private boolean updatePolicyEntry(String pid, String aid, String authrule) {
    //2. create policy index entry
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, pid);
    params.put(ACTION_ID, aid);
    params.put(AUTHRULE, authrule);

    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("updatePolicyEntry", params);
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"updatePolicyEntry()::", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//updatePolicyEntry

  private boolean removePolicyEntry(String pid, String aid) {
    //2. create policy index entry
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, pid);
    params.put(ACTION_ID, aid);

    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      lc.executeUpdate("removePolicyEntry", params);
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"updatePolicyEntry()::", ex);
      return false;
    } finally {
      m_SecurityStore.releaseConnection(lc);
    }
  }//removePolicyEntry

  private PolicyImpl loadPolicy(String pid) {
    Map<String,String> params = new HashMap<String,String>();
    params.put(POLICY_ID, pid);
    ResultSet rs = null;
    PolicyImpl p = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getPolicyEntries", params);
      p = new PolicyImpl(this, pid);
      while (rs.next()) {
        p.putEntry(new Action(rs.getString(1)), AuthorizationRuleImpl.create(rs.getString(2)));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"loadPolicy", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return p;
  }//loadPolicy

  private static Action LEASE_POLICY = new Action("leasePolicy");
  private static Action DELETE_POLICY = new Action("deletePolicy");
  private static Action CREATE_POLICY = new Action("createPolicy");
  private static Action PUT_ENTRY = new Action("putPolicyEntry");
  private static Action REMOVE_ENTRY = new Action("removePolicyEntry");
  private static Action[] ACTIONS =
      {LEASE_POLICY, CREATE_POLICY, DELETE_POLICY, REMOVE_ENTRY, PUT_ENTRY};


   /**/
  private static final String POLICY_ID = "policy_id";
  private static final String ACTION_ID = "action_id";
  private static final String AUTHRULE = "authrule";

}//class PolicyServiceImpl2
