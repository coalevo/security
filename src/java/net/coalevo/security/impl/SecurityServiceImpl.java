/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.foundation.util.crypto.UUIDGenerator;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.foundation.util.srp.StoreEntry;
import net.coalevo.security.model.*;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import net.coalevo.security.service.SecurityConfiguration;
import net.coalevo.security.service.SecurityService;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.util.*;

/**
 * Provides an implementation of a {@link SecurityService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SecurityServiceImpl
    extends BaseService
    implements SecurityService, Maintainable {

  private Marker m_LogMarker = MarkerFactory.getMarker(SecurityServiceImpl.class.getName());
  private Messages m_BundleMessages;

  private SecurityStore m_SecurityStore;
  private ServiceAgent m_ServiceAgent;
  private Policy m_Policy;

  private Map<Agent, Authorizations> m_AgentAuthorizations;
  private Map<Agent, Authorizations> m_ServiceAuthorizations;

  private LRUCacheMap<String, StoreEntry> m_AuthenticationsCache;
  private RoleInstanceCache m_RoleCache;
  private RolePermissionsCache m_RolePermissionsCache;
  private UserAgentInstanceCache m_UserAgentCache;
  private ServiceAgentInstanceCache m_ServiceAgentCache;
  private String m_ServiceUUID;

  public SecurityServiceImpl(SecurityStore st) {
    super(SecurityService.class.getName(), ACTIONS);
    m_SecurityStore = st;
  }//constructor

  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();
    //prepare cache div. maps and config update handler
    int authcachesize = 50;
    try {
      authcachesize = Activator.getServices().getConfigMediator().getConfiguration().getInteger(SecurityConfiguration.CONFIG_AUTHENTICATIONCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(
          m_BundleMessages.get("SecurityServiceImpl.config.exception", "attribute", SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY),
          ex
      );
    }

    m_AuthenticationsCache = new LRUCacheMap<String, StoreEntry>(authcachesize);
    Activator.getServices().getConfigMediator().addUpdateHandler(new ConfigurationUpdateHandler() {

      public void update(MetaTypeDictionary mtd) {
        try {
          m_AuthenticationsCache.setCeiling(mtd.getInteger(SecurityConfiguration.CONFIG_AUTHENTICATIONCACHE_KEY).intValue());
        } catch (MetaTypeDictionaryException ex) {
          Activator.log().error(
              m_BundleMessages.get("SecurityServiceImpl.config.exception", "attribute", SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY),
              ex
          );
        }
      }
    });
    m_ServiceUUID = UUIDGenerator.getUID();
    m_AgentAuthorizations = new HashMap<Agent, Authorizations>(50, 0.75f);
    m_ServiceAuthorizations = new HashMap<Agent, Authorizations>(50, 0.75f);
    m_RoleCache = m_SecurityStore.getRoleCache();
    m_RolePermissionsCache = m_SecurityStore.getRolePermissionsCache();
    m_UserAgentCache = m_SecurityStore.getUserAgentCache();
    m_ServiceAgentCache = m_SecurityStore.getServiceAgentCache();
    return true;
  }//activate

  public boolean activateSecurity(BundleContext bc) {
    //1. Authenticate
    try {
      m_ServiceAgent = authenticate(this);
    } catch (AuthenticationException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.activate.authentication", "service", "SecurityService"));
      return false;
    }

    //2. Obtain Policy
    PolicyService policyService = Activator.getServices().getPolicyService();
    if (policyService == null) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.reference", "service", "PolicyService"));
    }

    if (policyService.isPolicyAvailable(getIdentifier())) {
      m_Policy = policyService.leasePolicy(m_ServiceAgent, getIdentifier());
    } else {
      try {
        PolicyXMLService policyXMLService = Activator.getServices().getPolicyXMLService();
        if (policyService == null) {
          Activator.log().error(m_LogMarker, m_BundleMessages.get("error.reference", "service", "PolicyXMLService"));
        }
        //load from embedded file
        URL policy = bc.getBundle().getEntry("COALEVO-INF/security/SecurityService-policy.xml");
        Policy p = policyXMLService.fromXML(new InputStreamReader(policy.openStream()));
        //store it
        for (Iterator iterator = p.getActions().iterator(); iterator.hasNext();) {
          Action a = (Action) iterator.next();
          m_Policy = policyService.createPolicy(m_ServiceAgent, getIdentifier());
          boolean b = policyService.putPolicyEntry(m_ServiceAgent, m_Policy, a, p.getRuleFor(a));
          if (!b) {
            Activator.log().error(m_LogMarker, m_BundleMessages.get("error.policyrule", "action", a.getIdentifier()));
          }
        }
        Activator.log().info(m_LogMarker, m_BundleMessages.get(
            "SecurityServiceImpl.activation.policy",
            "service", "SecurityService",
            "file", policy.toString())
        );
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker, "activate()", ex);
        return false;
      }
    }
    return true;
  }//activateSecurity

  public boolean deactivate() {
    //remove the listener
    if (m_AgentAuthorizations != null) {
      m_AgentAuthorizations.clear();
      m_AgentAuthorizations = null;
    }
    if (m_AuthenticationsCache != null) {
      m_AuthenticationsCache.clear();
      m_AuthenticationsCache = null;
    }
    if (m_ServiceAuthorizations != null) {
      m_ServiceAuthorizations.clear();
      m_ServiceAuthorizations = null;
    }

    m_RolePermissionsCache = null;
    m_UserAgentCache = null;
    m_ServiceAgentCache = null;
    m_RoleCache = null;
    m_BundleMessages = null;
    return true;
  }//desactivate

  public ServerSRP getServerSRP(String id) {
    //1. check cache
    Object o = m_AuthenticationsCache.get(id);
    if (o != null) {
      return new InternalServerSRP(m_ServiceUUID, (StoreEntry) o);
    }
    //2. check store
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", id);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgentCredential", params);
      if (rs.next()) {
        //Get store entry
        StoreEntry se = new StoreEntry(id, rs.getString(1));
        //cache, create and return agent
        synchronized (m_AuthenticationsCache) {
          m_AuthenticationsCache.put(id, se);
        }
        return new InternalServerSRP(m_ServiceUUID, se);
      } else {
        return new InternalServerSRP(m_ServiceUUID, StoreEntry.createFakeEntry(id));
      }
    } catch (Exception ex) {
      Activator.log().error("authenticate()", ex);
      return null;
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
  }//getServerSRP

  public UserAgent getUserAgent(ServerSRP srp)
      throws SecurityException {
    if ((srp instanceof InternalServerSRP)
        && ((InternalServerSRP) srp).issuedBy(m_ServiceUUID)
        && (srp.isAuthenticated())
        ) {
      UserAgent a = m_UserAgentCache.getAgent(srp.getUsername());
      synchronized (m_AgentAuthorizations) {
        if (!m_AgentAuthorizations.containsKey(a)) {
          //System.err.println("Placing authorizations for " + a.getIdentifier());
          m_AgentAuthorizations.put(a, prepareAuthorizations(a));
        }
      }
      return a;
    } else {
      Activator.log().debug("getUserAgent()::" +
          ((srp instanceof InternalServerSRP) ? "instance" : "not-instance") +
          "::" +
          (((((InternalServerSRP) srp).issuedBy(m_ServiceUUID))) ? "issued" : "not-issued") +
          "::" +
          ((srp.isAuthenticated()) ? "authenticated" : "not-authenticated")
      );
      throw new SecurityException();
    }
  }//getUserAgent

  public UserAgent authenticate(AgentIdentifier aid, String credential)
      throws AuthenticationException {
    //1. shield from non-local identifiers
    if (!aid.isLocal()) {
      throw new IllegalArgumentException(m_BundleMessages.get("SecurityServiceImpl.authentication.nonlocal"));
    }
    //2. check authentication
    String id = aid.getName();

    if (cacheAuthenticateCT(id, credential) || storeAuthenticateCT(id, credential)) {
      //3. Cache will provide cached or new instance
      UserAgent a = m_UserAgentCache.getAgent(id);
      if (!m_AgentAuthorizations.containsKey(a)) {
        m_AgentAuthorizations.put(a, prepareAuthorizations(a));
      }
      return a;
    } else {
      throw new AuthenticationException();
    }
  }//authenticate

  public Set<String> authenticate(String id, String credential)
      throws AuthenticationException {
    //AgentIdentifier aid = AgentIdentifierInstanceCache.get(id);
    AgentIdentifier aid = new AgentIdentifier(id);
    if (!aid.isLocal()) {
      throw new IllegalArgumentException(m_BundleMessages.get("SecurityServiceImpl.authentication.nonlocal"));
    }
    id = aid.getName();
    if (cacheAuthenticateCT(id, credential) || storeAuthenticateCT(id, credential)) {
      //authenticated get roles
      return getAgentRolesByName(m_ServiceAgent, aid);
    } else {
      throw new AuthenticationException();
    }
  }//authenticate

  private boolean cacheAuthenticateCT(String id, String credential)
      throws AuthenticationException {
    Object o = null;
    synchronized (m_AuthenticationsCache) {
      o = m_AuthenticationsCache.get(id);
    }
    if (o != null) {
      //check against cache
      if (((StoreEntry) o).checkPassword(credential)) {
        return true;
      } else {
        throw new AuthenticationException(); //does not authenticate
      }
    } else {
      return false;
    }
  }//cacheAuthenticateCT

  private boolean storeAuthenticateCT(String id, String credential)
      throws AuthenticationException {
    //get from database
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", id);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgentCredential", params);
      if (rs.next()) {
        //Get store entry
        StoreEntry se = new StoreEntry(id, rs.getString(1));
        if (se.checkPassword(credential)) {
          //authenticated,cache, create and return agent
          synchronized (m_AuthenticationsCache) {
            m_AuthenticationsCache.put(id, se);
          }
          Activator.log().info(
              m_BundleMessages.get("SecurityServiceImpl.authentication.success", "aid", id)
          );
          return true;
        } else {
          Activator.log().info(
              m_BundleMessages.get("SecurityServiceImpl.authentication.failure", "aid", id)
          );
          throw new AuthenticationException();
        }
      } else {
        throw new AuthenticationException();
      }

    } catch (Exception ex) {
      Activator.log().error("authenticate()", ex);
      return false;
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
  }//storeAuthenticateCT

  private Authorizations prepareAuthorizations(Agent a) {
    String id = a.getAgentIdentifier().getName(); //local only!
    //2. Obtain Roles and Permissions
    RoleSet roles = new RoleSet(m_SecurityStore.getRoleCache());
    PermissionSet perms = new PermissionSet(
        m_SecurityStore.getPermitCache(),
        m_SecurityStore.getDenyCache());
    //Roles & Permissions
    Activator.log().debug("Loading roles for agent " + id);
    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", id);
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_SecurityStore.leaseConnection();
      rs = lc.executeQuery("getAgentRoles", params);
      while (rs.next()) {
        RoleImpl r = m_RoleCache.getRole(rs.getInt(1), rs.getString(2));
        Activator.log().debug("Loading Role " + r.getNumber() + " " + r.toString());
        perms.addAll(m_RolePermissionsCache.getPermissions(r));
        roles.add(r);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepareAuthorization()", ex);
    } finally {
      SqlUtils.close(rs);
      m_SecurityStore.releaseConnection(lc);
    }
    return new AuthorizationsImpl(a, roles, perms);
  }//prepareAuthorizations

  public ServiceAgent authenticate(Service s) throws AuthenticationException {
    for (Iterator iterator = m_ServiceAuthorizations.keySet().iterator(); iterator.hasNext();) {
      Agent a = (Agent) iterator.next();
      if (a.getAgentIdentifier().getName().equals(s.getIdentifier())) {
        throw new AuthenticationException();
      }
    }
    //2. add agent to authenticated list
    ServiceAgentImpl sa = m_ServiceAgentCache.getAgent(s.getIdentifier());
    m_ServiceAuthorizations.put(sa, prepareAuthorizations(sa));
    //Activator.log().debug("Authenticated " + sa.getIdentifier() +"::" +  sa.toString());
    return sa;
  }//authenticate

  public boolean invalidateAuthentication(Agent a) {
    if (a == null) {
      Activator.log().error("invalidateAuthentication(Agent)::NULL");
      return false;
    }
    if (a instanceof ServiceAgentImpl) {
      if (m_ServiceAuthorizations == null) {
        return true;
      }
      synchronized (m_ServiceAuthorizations) {
        return (m_ServiceAuthorizations.remove(a) != null);
      }
    } else {
      if (m_AgentAuthorizations == null) {
        return true;
      }
      synchronized (m_AgentAuthorizations) {
        return (m_AgentAuthorizations.remove(a) != null);
      }
    }
  }//invalidateAuthentication

  public boolean isAuthentic(Agent a) {
    if (a instanceof ServiceAgentImpl) {
      synchronized (m_ServiceAuthorizations) {
        return m_ServiceAuthorizations.containsKey(a);
      }
    } else {
      synchronized (m_AgentAuthorizations) {
        return m_AgentAuthorizations.containsKey(a);
      }
    }
  }//isAuthentic

  public void checkAuthentic(Agent a) throws SecurityException {
    if (!isAuthentic(a)) {
      throw new AuthenticationException(a.getIdentifier());
    }
  }//checkAuthentic

  public Set<Role> getAgentRoles(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException {
    Set<Role> roles = new HashSet<Role>();
    for (String role : getAgentRolesByName(caller, aid)) {
      roles.add(m_RoleCache.getRole(role));
    }
    return Collections.unmodifiableSet(roles);
  }//getAgentRoles

  public Set<String> getAgentRolesByName(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAgentException {

    checkAuthentic(caller);
    Object o = null;
    String name = aid.getName();
    if (m_UserAgentCache.contains(name)) {
      synchronized (m_AgentAuthorizations) {
        o = m_AgentAuthorizations.get(m_UserAgentCache.getAgent(aid.getName()));
      }
    } else if (m_ServiceAgentCache.contains(name)) {
      synchronized (m_ServiceAuthorizations) {
        o = m_ServiceAuthorizations.get(m_ServiceAgentCache.getAgent(name));
      }
    }
    if (o != null) {
      return ((AuthorizationsImpl) o).getRolenames();
    } else {
      //need to get uncached auths
      Set<String> s = new HashSet<String>();
      Map<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getName());
      ResultSet rs = null;
      LibraryConnection lc = null;
      try {
        lc = m_SecurityStore.leaseConnection();
        rs = lc.executeQuery("getAgentRoles", params);
        while (rs.next()) {
          s.add(rs.getString(2));
        }
      } catch (Exception ex) {
        Activator.log().error("getAgentRoles()", ex);
        throw new NoSuchAgentException(aid.getIdentifier());
      } finally {
        SqlUtils.close(rs);
        m_SecurityStore.releaseConnection(lc);
      }
      return Collections.unmodifiableSet(s);
    }
  }//getAgentRoles


  public Authorizations getAuthorizations(Agent a)
      throws SecurityException {
    Object o = null;
    if (a instanceof ServiceAgentImpl) {
      synchronized (m_ServiceAuthorizations) {
        o = m_ServiceAuthorizations.get(a);
      }
    } else {
      synchronized (m_AgentAuthorizations) {
        o = m_AgentAuthorizations.get(a);
      }
    }
    if (o != null) {
      return (Authorizations) o;
    } else {
      throw new AuthenticationException(a.getIdentifier());
    }
  }//getAuthorizations

  /**
   * Updates all online authorizations, freezing momentarily
   * all access to them.
   * This should probably be run asynchronously through the
   * ExecutionService.
   *
   * @param smi the {@link ServiceAgent}
   * @param r   the {@link Role} to be updated.
   */
  public void updateAuthorizations(ServiceAgent smi, Role r) {
    if (isAuthentic(smi)) {
      Activator.log().debug("updateAuthorizations()::" + r.getIdentifier());
      //1. Invalidate Cache
      m_RolePermissionsCache.invalidate(r);
      //2. iterate over authorizations to update them
      synchronized (m_AgentAuthorizations) {
        for (Iterator iter = m_AgentAuthorizations.keySet().iterator(); iter.hasNext();) {
          Agent a = (Agent) iter.next();
          //replace authorizations with new ones
          m_AgentAuthorizations.put(a, prepareAuthorizations(a));
        }
      }
    }
  }//updateAuthorizations

  /**
   * Updates all online authorizations, freezing momentarily
   * all access to them. Note that this will purge the cache.
   * This should probably be run asynchronously through the
   * ExecutionService.
   *
   * @param smi the {@link ServiceAgent}
   */
  public void updateAuthorizations(ServiceAgent smi) {
    if (isAuthentic(smi)) {
      //1. Invalidate Cache
      m_RolePermissionsCache.invalidateAll();
      //2. iterate over authorizations to update them
      synchronized (m_AgentAuthorizations) {
        for (Iterator iter = m_AgentAuthorizations.keySet().iterator(); iter.hasNext();) {
          Agent a = (Agent) iter.next();
          //replace authorizations with new ones
          m_AgentAuthorizations.put(a, prepareAuthorizations(a));
        }
      }
    }
  }//updateAuthorizations

  /**
   * Updates all online authorizations, freezing momentarily
   * all access to them.
   * This should probably be run asynchronously through the
   * ExecutionService.
   *
   * @param smi the {@link ServiceAgent}
   * @param a   the {@link Agent} to be updated
   */
  public void updateAuthorizations(ServiceAgent smi, Agent a) {
    if (isAuthentic(smi)) {
      //2. update agent authorizations
      synchronized (m_AgentAuthorizations) {
        if (m_AgentAuthorizations.containsKey(a)) {
          //replace authorizations with new ones
          m_AgentAuthorizations.put(a, prepareAuthorizations(a));
        }
      }
    }
  }//updateAuthorizations

  public void invalidateCachedCredential(Agent a) {
    m_AuthenticationsCache.remove(a.getAgentIdentifier().getName());
  }//invalidateCachedCredential

  public void doMaintenance(Agent caller) throws SecurityException, MaintenanceException {
    try {
      m_Policy.ensureAuthorization(getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }
    try {
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.start"));
      //maintain store
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.store"));
      m_SecurityStore.maintain(m_ServiceAgent);

      //maintain caches
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.caches"));
      m_AuthenticationsCache.clear();

      //update authorizations, caches have been flushed by store maintenance
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.agentauthorizations"));
      synchronized (m_AgentAuthorizations) {
        for (Iterator iter = m_AgentAuthorizations.keySet().iterator(); iter.hasNext();) {
          Agent a = (Agent) iter.next();
          //replace authorizations with new ones
          m_AgentAuthorizations.put(a, prepareAuthorizations(a));
        }
      }
      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.do.serviceauthorizations"));
      synchronized (m_ServiceAuthorizations) {
        for (Iterator iter = m_ServiceAuthorizations.keySet().iterator(); iter.hasNext();) {
          ServiceAgent a = (ServiceAgent) iter.next();
          //replace authorizations with new ones
          m_ServiceAuthorizations.put(a, prepareAuthorizations(a));
        }
      }

      Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
    } catch (Exception ex) {
      throw new MaintenanceException(ex.getMessage(), ex);
    }
  }//doMaintenance

  private static Action[] ACTIONS = {Maintainable.DO_MAINTENANCE};

}//class SecurityServiceImpl
