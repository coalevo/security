/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.security.model.NoSuchRoleException;
import net.coalevo.security.model.Role;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Provides a package local implementation of a set of roles.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class RoleSet {

  private Set<Role> m_Roles;
  private RoleInstanceCache m_RoleCache;

  public RoleSet(RoleInstanceCache rc) {
    m_Roles = new HashSet<Role>();
    m_RoleCache = rc;
  }//constructor

  public void add(Role r) {
    m_Roles.add(r);
  }//add

  public void remove(Role r) {
    m_Roles.remove(r);
  }//remove

  public Iterator iterator() {
    return m_Roles.iterator();
  }//iterator

  public boolean contains(String id) {
    try {
      return m_Roles.contains(m_RoleCache.getRole(id));
    } catch (NoSuchRoleException ex) {
      return false;
    }
  }//contains

  public boolean contains(Role r) {
    return m_Roles.contains(r);
  }//contains

}//class RoleSet
