// $ANTLR 2.7.5 (20050128): "sarl.g" -> "AuthorizationRuleLexer.java"$

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import antlr.*;

import java.io.InputStream;
import java.io.Reader;
import java.util.Hashtable;

/**
 * Provides a <tt>Lexer</tt> for SARL that
 * was automatically generated from a grammar
 * by ANTLR.
 *
 * @author Dieter Wimberger (grammar)
 * @author ANTLR
 */
class AuthorizationRuleLexer extends antlr.CharScanner implements AuthorizationRuleLexerTokenTypes, TokenStream {

  public AuthorizationRuleLexer(InputStream in) {
    this(new ByteBuffer(in));
  }

  public AuthorizationRuleLexer(Reader in) {
    this(new CharBuffer(in));
  }

  public AuthorizationRuleLexer(InputBuffer ib) {
    this(new LexerSharedInputState(ib));
  }

  public AuthorizationRuleLexer(LexerSharedInputState state) {
    super(state);
    caseSensitiveLiterals = true;
    setCaseSensitive(true);
    literals = new Hashtable();
  }

  public Token nextToken() throws TokenStreamException {
    Token theRetToken = null;
    tryAgain:
    for (; ;) {
      Token _token = null;
      int _ttype = Token.INVALID_TYPE;
      resetText();
      try {   // for char stream error handling
        try {   // for lexical error handling
          switch (LA(1)) {
            case'-':
            case'.':
            case'0':
            case'1':
            case'2':
            case'3':
            case'4':
            case'5':
            case'6':
            case'7':
            case'8':
            case'9':
            case'A':
            case'B':
            case'C':
            case'D':
            case'E':
            case'F':
            case'G':
            case'H':
            case'I':
            case'J':
            case'K':
            case'L':
            case'M':
            case'N':
            case'O':
            case'P':
            case'Q':
            case'R':
            case'S':
            case'T':
            case'U':
            case'V':
            case'W':
            case'X':
            case'Y':
            case'Z':
            case'_':
            case'a':
            case'b':
            case'c':
            case'd':
            case'e':
            case'f':
            case'g':
            case'h':
            case'i':
            case'j':
            case'k':
            case'l':
            case'm':
            case'n':
            case'o':
            case'p':
            case'q':
            case'r':
            case's':
            case't':
            case'u':
            case'v':
            case'w':
            case'x':
            case'y':
            case'z': {
              mPERMISSION(true);
              theRetToken = _returnToken;
              break;
            }
            case'%': {
              mROLE(true);
              theRetToken = _returnToken;
              break;
            }
            case'&': {
              mAND(true);
              theRetToken = _returnToken;
              break;
            }
            case'|': {
              mOR(true);
              theRetToken = _returnToken;
              break;
            }
            case'(': {
              mLPAREN(true);
              theRetToken = _returnToken;
              break;
            }
            case')': {
              mRPAREN(true);
              theRetToken = _returnToken;
              break;
            }
            case';': {
              mSEMI(true);
              theRetToken = _returnToken;
              break;
            }
            case'\t':
            case'\n':
            case'\r':
            case' ': {
              mWS(true);
              theRetToken = _returnToken;
              break;
            }
            default: {
              if (LA(1) == EOF_CHAR) {
                uponEOF();
                _returnToken = makeToken(Token.EOF_TYPE);
              } else {
                throw new NoViableAltForCharException((char) LA(1), getFilename(), getLine(), getColumn());
              }
            }
          }
          if (_returnToken == null) continue tryAgain; // found SKIP token
          _ttype = _returnToken.getType();
          _ttype = testLiteralsTable(_ttype);
          _returnToken.setType(_ttype);
          return _returnToken;
        }
        catch (RecognitionException e) {
          throw new TokenStreamRecognitionException(e);
        }
      }
      catch (CharStreamException cse) {
        if (cse instanceof CharStreamIOException) {
          throw new TokenStreamIOException(((CharStreamIOException) cse).io);
        } else {
          throw new TokenStreamException(cse.getMessage());
        }
      }
    }
  }

  public final void mPERMISSION(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = PERMISSION;
    int _saveIndex;

    {
      int _cnt3 = 0;
      _loop3:
      do {
        switch (LA(1)) {
          case'a':
          case'b':
          case'c':
          case'd':
          case'e':
          case'f':
          case'g':
          case'h':
          case'i':
          case'j':
          case'k':
          case'l':
          case'm':
          case'n':
          case'o':
          case'p':
          case'q':
          case'r':
          case's':
          case't':
          case'u':
          case'v':
          case'w':
          case'x':
          case'y':
          case'z': {
            matchRange('a', 'z');
            break;
          }
          case'A':
          case'B':
          case'C':
          case'D':
          case'E':
          case'F':
          case'G':
          case'H':
          case'I':
          case'J':
          case'K':
          case'L':
          case'M':
          case'N':
          case'O':
          case'P':
          case'Q':
          case'R':
          case'S':
          case'T':
          case'U':
          case'V':
          case'W':
          case'X':
          case'Y':
          case'Z': {
            matchRange('A', 'Z');
            break;
          }
          case'0':
          case'1':
          case'2':
          case'3':
          case'4':
          case'5':
          case'6':
          case'7':
          case'8':
          case'9': {
            matchRange('0', '9');
            break;
          }
          case'_': {
            match('_');
            break;
          }
          case'-': {
            match('-');
            break;
          }
          case'.': {
            match('.');
            break;
          }
          default: {
            if (_cnt3 >= 1) {
              break _loop3;
            } else {
              throw new NoViableAltForCharException((char) LA(1), getFilename(), getLine(), getColumn());
            }
          }
        }
        _cnt3++;
      } while (true);
    }
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mROLE(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = ROLE;
    int _saveIndex;

    match('%');
    {
      int _cnt6 = 0;
      _loop6:
      do {
        switch (LA(1)) {
          case'a':
          case'b':
          case'c':
          case'd':
          case'e':
          case'f':
          case'g':
          case'h':
          case'i':
          case'j':
          case'k':
          case'l':
          case'm':
          case'n':
          case'o':
          case'p':
          case'q':
          case'r':
          case's':
          case't':
          case'u':
          case'v':
          case'w':
          case'x':
          case'y':
          case'z': {
            matchRange('a', 'z');
            break;
          }
          case'A':
          case'B':
          case'C':
          case'D':
          case'E':
          case'F':
          case'G':
          case'H':
          case'I':
          case'J':
          case'K':
          case'L':
          case'M':
          case'N':
          case'O':
          case'P':
          case'Q':
          case'R':
          case'S':
          case'T':
          case'U':
          case'V':
          case'W':
          case'X':
          case'Y':
          case'Z': {
            matchRange('A', 'Z');
            break;
          }
          case'0':
          case'1':
          case'2':
          case'3':
          case'4':
          case'5':
          case'6':
          case'7':
          case'8':
          case'9': {
            matchRange('0', '9');
            break;
          }
          case'_': {
            match('_');
            break;
          }
          case'-': {
            match('-');
            break;
          }
          case'.': {
            match('.');
            break;
          }
          default: {
            if (_cnt6 >= 1) {
              break _loop6;
            } else {
              throw new NoViableAltForCharException((char) LA(1), getFilename(), getLine(), getColumn());
            }
          }
        }
        _cnt6++;
      } while (true);
    }
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mAND(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = AND;
    int _saveIndex;

    match('&');
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mOR(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = OR;
    int _saveIndex;

    match('|');
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mLPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = LPAREN;
    int _saveIndex;

    match('(');
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mRPAREN(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = RPAREN;
    int _saveIndex;

    match(')');
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mSEMI(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = SEMI;
    int _saveIndex;

    match(';');
    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }

  public final void mWS(boolean _createToken) throws RecognitionException, CharStreamException, TokenStreamException {
    int _ttype;
    Token _token = null;
    int _begin = text.length();
    _ttype = WS;
    int _saveIndex;

    {
      switch (LA(1)) {
        case' ': {
          match(' ');
          break;
        }
        case'\t': {
          match('\t');
          break;
        }
        case'\r': {
          match('\r');
          match('\n');
          newline();
          break;
        }
        case'\n': {
          match('\n');
          newline();
          break;
        }
        default: {
          throw new NoViableAltForCharException((char) LA(1), getFilename(), getLine(), getColumn());
        }
      }
    }

    _ttype = Token.SKIP;

    if (_createToken && _token == null && _ttype != Token.SKIP) {
      _token = makeToken(_ttype);
      _token.setText(new String(text.getBuffer(), _begin, text.length() - _begin));
    }
    _returnToken = _token;
  }


}
