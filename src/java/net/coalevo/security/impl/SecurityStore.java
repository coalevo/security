/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.MaintenanceException;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.foundation.util.srp.StoreEntry;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.security.service.SecurityConfiguration;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.io.File;

/**
 * Provides a store implementation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class SecurityStore
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(SecurityStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;

  // Caches
  private RoleInstanceCache m_RoleCache;
  private BasePermissionInstanceCache m_PermissionCache;
  private BasePermissionInstanceCache m_PermitCache;
  private BasePermissionInstanceCache m_DenyCache;
  private RolePermissionsCache m_RolePermissionsCache;
  private UserAgentInstanceCache m_UserAgentCache;
  private ServiceAgentInstanceCache m_ServiceAgentCache;

  // Config values
  String m_Root = "tgitm";
  String m_AdminRole = "Admin";
  String m_UserRole = "User";

  public boolean isNew() {
    return m_New;
  }//isNew

  public RoleInstanceCache getRoleCache() {
    return m_RoleCache;
  }//getRoleCache

  public BasePermissionInstanceCache getPermissionCache() {
    return m_PermissionCache;
  }//getPermissionCache

  public BasePermissionInstanceCache getPermitCache() {
    return m_PermitCache;
  }//getPermitCache

  public BasePermissionInstanceCache getDenyCache() {
    return m_DenyCache;
  }//getDenyCache

  public RolePermissionsCache getRolePermissionsCache() {
    return m_RolePermissionsCache;
  }//getRolePermissionsCache

  public UserAgentInstanceCache getUserAgentCache() {
    return m_UserAgentCache;
  }//getUserAgentCache

  public ServiceAgentInstanceCache getServiceAgentCache() {
    return m_ServiceAgentCache;
  }//getServiceAgentCache

  public String getBaseAdminRoleName() {
    return m_AdminRole;
  }//getBaseAdminRoleName

  public String getBaseUserRoleName() {
    return m_UserRole;
  }//getBaseUserRoleName

  public String getBaseAdminAuthorizationRule() {
    return new StringBuilder("(").append('%').append(getBaseAdminRoleName()).append("| %Service);").toString();
  }//getBaseAuthorizationRule

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    return (LibraryConnection) m_ConnectionPool.borrowObject();
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
    } catch (Exception e) {
      Activator.log().error(m_LogMarker, "releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSecuritySchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }

    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private synchronized void createSchema(LibraryConnection lc)
      throws Exception {

    lc = leaseConnection();
    //1. Create schemas
    lc.executeCreate("createSecuritySchema");
    lc.executeCreate("createPoliciesTable");
    lc.executeCreate("createPolicyEntriesTable");
    lc.executeCreate("createAgentsTable");
    lc.executeCreate("createAgentCredentialsTable");
    lc.executeCreate("createRolesTable");
    lc.executeCreate("createAgentRolesTable");
    lc.executeCreate("createPermissionsTable");
    lc.executeCreate("createRolePermissionsTable");
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("SecurityStore.setup.schemas"));

    Map<String, String> params = new HashMap<String, String>();

    //2. Register base agent (including services) entries
    AgentIdentifier aid = new AgentIdentifier(m_Root);
    params.put("agent_type", "1");
    params.put("agent_id", aid.getName());
    lc.executeUpdate("registerAgent", params);


    //prepare initialization
    File f = new File(System.getProperty("coalevo.security.initfile","./configuration/security-init.properties"));
    SecurityInit si = SecurityInit.create(f);

   //Setup security itself
    params.put("agent_type", "0");
    params.put("agent_id", SecurityStore.class.getName());
    lc.executeUpdate("registerAgent", params);
    params.put("agent_id", SecurityService.class.getName());
    lc.executeUpdate("registerAgent", params);
    params.put("agent_id", PolicyService.class.getName());
    lc.executeUpdate("registerAgent", params);
    params.put("agent_id", SecurityManagementService.class.getName());
    lc.executeUpdate("registerAgent", params);

    //setup init services
    for(String service:si.getServices()) {
      params.put("agent_id", service);
      lc.executeUpdate("registerAgent", params);
    }

    //3. Add credential for root agent entry
    String rootpasswd = Activator.getServices()
        .getRndStringGeneratorService(ServiceMediator.WAIT_UNLIMITED).getRandomString(10);
    params.clear();
    params.put("agent_id", aid.getName());
    //params.put("type_id", "1");
    StoreEntry se = StoreEntry.createStoreEntry(aid.getName(), rootpasswd);
    params.put("credential", se.toString());
    lc.executeUpdate("createAgentCredential", params);
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("SecurityStore.setup.root", "username", m_Root, "password", rootpasswd));
    System.out.println(Activator.getBundleMessages().get("SecurityStore.setup.root", "username", m_Root, "password", rootpasswd));

    //4. Create and grant base roles
    params.clear();
    params.put("name", m_AdminRole);
    lc.executeUpdate("createRole", params); //1!
    params.put("name", "Service");
    lc.executeUpdate("createRole", params); //2!
    params.put("name", m_UserRole);
    lc.executeUpdate("createRole", params); //3!

    //Setup init roles
    for(String role:si.getRoles()) {
      params.put("name", role);
      lc.executeUpdate("createRole", params);
    }

    //Roles for root
    params.clear();
    params.put("agent_id", aid.getName());
    params.put("role_id", "1");
    lc.executeUpdate("grantRole", params);
    params.put("role_id", "3");
    lc.executeUpdate("grantRole", params);

    //Roles for security services
    params.put("agent_id", SecurityStore.class.getName());
    params.put("role_id", "2");
    lc.executeUpdate("grantRole", params);
    params.put("agent_id", SecurityService.class.getName());
    lc.executeUpdate("grantRole", params);
    params.put("agent_id", PolicyService.class.getName());
    lc.executeUpdate("grantRole", params);
    params.put("agent_id", SecurityManagementService.class.getName());
    lc.executeUpdate("grantRole", params);

    //Init role for services
    for(String service:si.getServices()) {
      params.put("agent_id", service);
      lc.executeUpdate("grantRole", params);
    }
  }//createDatabase

  public void maintain(ServiceAgent agent) throws MaintenanceException {

    //maintain caches
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.caches"));
    m_RoleCache.clear();
    m_PermissionCache.clear();
    m_PermitCache.clear();
    m_DenyCache.clear();
    m_RolePermissionsCache.clear();
    m_UserAgentCache.clear();
    m_ServiceAgentCache.clear();
  }//doMaintenance

  public boolean activate() {
    //1. Configure cache sizes
    String ds = "default";
    int cpoolsize = 5;
    int rcachesize = 50;
    int pcachesize = 50;
    int pmcachesize = 50;
    int dcachesize = 50;
    int rpcachesize = 25;
    int uacachesize = 50;
    int sacachesize = 25;
    MetaTypeDictionary mtd = Activator.getServices().getConfigMediator().getConfiguration();
    Activator.log().debug(m_LogMarker, "activate(BundleContext)::" + mtd.getDictionary().toString());
    try {
      ds = mtd.getString(SecurityConfiguration.CONFIG_DATASOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_DATASOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(SecurityConfiguration.CONFIG_CONNECTIONPOOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_CONNECTIONPOOLSIZE_KEY),
          ex
      );
    }
    try {
      rcachesize = mtd.getInteger(SecurityConfiguration.CONFIG_ROLECACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ROLECACHE_KEY),
          ex
      );
    }
    try {
      pcachesize = mtd.getInteger(SecurityConfiguration.CONFIG_PERMISSIONCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_PERMISSIONCACHE_KEY),
          ex
      );
    }
    try {
      pmcachesize = mtd.getInteger(SecurityConfiguration.CONFIG_PERMITCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_PERMITCACHE_KEY),
          ex
      );
    }
    try {
      dcachesize = mtd.getInteger(SecurityConfiguration.CONFIG_DENYCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_DENYCACHE_KEY),
          ex
      );
    }
    try {
      rpcachesize = mtd.getInteger(SecurityConfiguration.CONFIG_ROLEPERMISSIONCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ROLEPERMISSIONCACHE_KEY),
          ex
      );
    }
    try {
      uacachesize = mtd.getInteger(SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY),
          ex
      );
    }
    try {
      sacachesize = mtd.getInteger(SecurityConfiguration.CONFIG_SERVICEAGENTCACHE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_SERVICEAGENTCACHE_KEY),
          ex
      );
    }
    try {
      m_Root = mtd.getString(SecurityConfiguration.CONFIG_ROOT_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ROOT_KEY),
          ex
      );
    }
    try {
      m_AdminRole = mtd.getString(SecurityConfiguration.CONFIG_ADMINROLE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ADMINROLE_KEY),
          ex
      );
    }
    try {
      m_UserRole = mtd.getString(SecurityConfiguration.CONFIG_USERROLE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_USERROLE_KEY),
          ex
      );
    }

    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(SecurityStore.class, "net/coalevo/security/impl/securitystore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, "activate()", ex);
      return false;
    }
    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.datasource"),
          nse
      );
      return false;
    }

    //create caches
    m_RoleCache = RoleInstanceCache.createRoleCache(SecurityStore.this, rcachesize);
    m_PermissionCache = new PermissionInstanceCache(SecurityStore.this, pcachesize);
    m_PermitCache = new PermitInstanceCache(SecurityStore.this, pmcachesize);
    m_DenyCache = new DenyInstanceCache(SecurityStore.this, dcachesize);

    m_RolePermissionsCache = RolePermissionsCache.createRolePermissionsCache(SecurityStore.this, rpcachesize);
    m_UserAgentCache = UserAgentInstanceCache.createAgentCache(SecurityStore.this, uacachesize);
    m_ServiceAgentCache = ServiceAgentInstanceCache.createAgentCache(SecurityStore.this, sacachesize);
    //add update handler
    Activator.getServices().getConfigMediator().addUpdateHandler(this);

    //prepare store
    prepareDataSource();

    Activator.log().info(m_LogMarker,
        Activator.getBundleMessages().get("SecurityStore.database.info", "source", m_DataSource.toString())
    );
    return true;
  }//activate

  public synchronized boolean deactivate() {
    //1. close all leased connections?
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(m_LogMarker, "deactivate()", e);
      }
    }
    //2. clear caches?
    if (m_RoleCache != null) {
      m_RoleCache.clear();
    }
    m_RoleCache = null;
    if (m_PermissionCache != null) {
      m_PermissionCache.clear();
    }
    m_PermissionCache = null;
    if (m_PermitCache != null) {
      m_PermitCache.clear();
    }
    m_PermitCache = null;
    if (m_DenyCache != null) {
      m_DenyCache.clear();
    }
    m_DenyCache = null;
    if (m_RolePermissionsCache != null) {
      m_RolePermissionsCache.clear();
    }
    m_RolePermissionsCache = null;
    if (m_UserAgentCache != null) {
      m_UserAgentCache.clear();
    }
    m_UserAgentCache = null;
    if (m_ServiceAgentCache != null) {
      m_ServiceAgentCache.clear();
    }
    m_ServiceAgentCache = null;

    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary md) {

    Activator.log().debug(m_LogMarker, "update(MetaTypeDictionary)::" + md.getDictionary().toString());
    try {
      final int cpsize = md.getInteger(SecurityConfiguration.CONFIG_CONNECTIONPOOLSIZE_KEY).intValue();
      m_ConnectionPool.setMaxActive(cpsize);
      m_ConnectionPool.setMaxIdle(cpsize);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_CONNECTIONPOOLSIZE_KEY),
          ex
      );
    }
    try {
      m_RoleCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_ROLECACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ROLECACHE_KEY),
          ex
      );
    }
    try {
      m_PermissionCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_PERMISSIONCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_PERMISSIONCACHE_KEY),
          ex
      );
    }
    try {
      m_PermitCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_PERMITCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_PERMITCACHE_KEY),
          ex
      );
    }
    try {
      m_DenyCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_DENYCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_DENYCACHE_KEY),
          ex
      );
    }
    try {
      m_RolePermissionsCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_ROLEPERMISSIONCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_ROLEPERMISSIONCACHE_KEY),
          ex
      );
    }
    try {
      m_UserAgentCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_USERAGENTCACHE_KEY),
          ex
      );
    }
    try {
      m_ServiceAgentCache.setCacheSize(md.getInteger(SecurityConfiguration.CONFIG_SERVICEAGENTCACHE_KEY).intValue());
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("SecurityStore.activation.configexception", "attribute", SecurityConfiguration.CONFIG_SERVICEAGENTCACHE_KEY),
          ex
      );
    }
  }//update

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate", null);
      } catch (Exception ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class SecurityStore
