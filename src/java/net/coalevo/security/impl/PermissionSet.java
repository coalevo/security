/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.security.model.Deny;
import net.coalevo.security.model.Permission;
import net.coalevo.security.model.Permit;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Provides a package local implementation of a set of permissions.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PermissionSet {

  private final Set<Permission> m_Permissions;
  private BasePermissionInstanceCache m_PermitCache;
  private BasePermissionInstanceCache m_DenyCache;
  private boolean m_Invalid;

  public PermissionSet(BasePermissionInstanceCache permit,
                       BasePermissionInstanceCache deny) {
    m_Permissions = new HashSet<Permission>();
    m_PermitCache = permit;
    m_DenyCache = deny;
  }//constructor

  public void invalidate() {
    m_Invalid = true;
  }//invalidate

  public boolean isValid() {
    return !m_Invalid;
  }//isValid

  public Iterator iterator() {
    return m_Permissions.iterator();
  }//iterator

  public void add(Permission p) {
    //this seems like silly code, but it shouldn't be actually :)
    //It replaces a permit with a deny if added.
    if (!m_Permissions.add(p) && p instanceof Deny) {
      m_Permissions.remove(p);
      m_Permissions.add(p);
    }
  }//add

  public void addAll(PermissionSet p) {
    for (Iterator iterator = p.iterator(); iterator.hasNext();) {
      add((Permission) iterator.next());
    }
  }//addAll

  public void add(int num, String id, boolean permit) {
    if (permit) {
      add(m_PermitCache.getPermission(num, id));
    } else {
      add(m_DenyCache.getPermission(num, id));
    }
  }//add

  public void remove(Permission p) {
    m_Permissions.remove(p);
  }//remove

  public boolean check(Permission p) {
    return check(p.getIdentifier());
  }//check

  public boolean check(String id) {
    for (Iterator iterator = m_Permissions.iterator(); iterator.hasNext();) {
      Permission p1 = (Permission) iterator.next();
      if (p1.getIdentifier().equals(id)) {
        if (p1 instanceof Permit) {
          return true;
        } else if (p1 instanceof Deny) {
          return false;
        }
      }
    }
    return false;
  }//check

  public Set<Permission> getAsUnmodifiableSet() {
    return Collections.unmodifiableSet(m_Permissions);
  }//getAsUnmodifiableSet

  public String toString() {
    return m_Permissions.toString();
  }//toString

}//class PermissionSet
