/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.security.impl;

import net.coalevo.foundation.model.Agent;
import net.coalevo.security.model.Authorizations;
import net.coalevo.security.model.Role;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;


/**
 * Provides a package local implementation of {@link Authorizations}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AuthorizationsImpl
    implements Authorizations {

  private Agent m_Agent;
  private RoleSet m_Roles;
  private PermissionSet m_Permissions;
  private Set<String> m_Rolenames;

  public AuthorizationsImpl(Agent a, RoleSet roles, PermissionSet perms) {
    m_Agent = a;
    m_Roles = roles;
    m_Permissions = perms;
  }//AuthorizationsImpl

  public boolean hasPermission(String id) {
    return m_Permissions.check(id);
  }//hasPermission

  public boolean hasRole(String id) {
    return m_Roles.contains(id);
  }//hasRole

  public Set<String> getRolenames() {
    if (m_Rolenames == null) {
      m_Rolenames = new TreeSet<String>();
      for (Iterator iter = m_Roles.iterator(); iter.hasNext();) {
        m_Rolenames.add(((Role) iter.next()).getIdentifier());
      }
      m_Rolenames = Collections.unmodifiableSet(m_Rolenames);
    }
    return m_Rolenames;
  }//getRolesnames

  public boolean isAgent(Agent a) {
    return a!=null && a.equals(m_Agent);
  }//isAgent
  
}//class AuthorizationsImpl
